/*
 **
 ** Source file generated on August 26, 2016 at 15:09:15.	
 **
 ** Copyright (C) 2016 Analog Devices Inc., All Rights Reserved.
 **
 ** This file is generated automatically based upon the options selected in 
 ** the Pin Multiplexing configuration editor. Changes to the Pin Multiplexing
 ** configuration should be made by changing the appropriate options rather
 ** than editing this file.
 **
 ** Selected Peripherals
 ** --------------------
 ** SPI0 (SCLK, MOSI, MISO, CS_0, CS_1, CS_2)
 ** SPI1 (SCLK, MISO, MOSI, CS_0)
 ** I2C0 (SCL0, SDA0)
 ** UART0 (Tx, Rx)
 ** SYS_WAKEUP (sys_wake0, sys_wake1, sys_wake2, sys_wake3)
 ** SWD0 (SWD0_CLK, SWD0_Data)
 ** ADC0_IN (ADC0_IN0, ADC0_IN1)
 **
 ** GPIO (unavailable)
 ** ------------------
 ** P0_00, P0_01, P0_02, P0_03, P0_04, P0_05, P0_06, P0_07, P0_10, P0_11, P0_13, P0_15,
 ** P1_00, P1_06, P1_07, P1_08, P1_09, P1_10, P2_01, P2_03, P2_04, P2_08
 */

#include <sys/platform.h>
#include <stdint.h>

#define SPI0_SCLK_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<0))
#define SPI0_MOSI_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<2))
#define SPI0_MISO_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<4))
//#define SPI0_CS_0_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<6))
#define SPI0_CS_1_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<20))
//#define SPI0_CS_2_PORTP2_MUX  ((uint32_t) ((uint32_t) 2<<16))
#define SPI1_SCLK_PORTP1_MUX  ((uint16_t) ((uint16_t) 1<<12))
#define SPI1_MISO_PORTP1_MUX  ((uint16_t) ((uint16_t) 1<<14))
#define SPI1_MOSI_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<16))
//#define SPI1_CS_0_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<18))
#define I2C0_SCL0_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<8))
#define I2C0_SDA0_PORTP0_MUX  ((uint16_t) ((uint16_t) 1<<10))
#define UART0_TX_PORTP0_MUX  ((uint32_t) ((uint32_t) 1<<20))
#define UART0_RX_PORTP0_MUX  ((uint32_t) ((uint32_t) 1<<22))
//#define SYS_WAKEUP_SYS_WAKE0_PORTP0_MUX  ((uint32_t) ((uint32_t) 1<<30))
//#define SYS_WAKEUP_SYS_WAKE1_PORTP1_MUX  ((uint16_t) ((uint16_t) 1<<0))
#define SYS_WAKEUP_SYS_WAKE2_PORTP0_MUX  ((uint32_t) ((uint32_t) 1<<26))
#define SYS_WAKEUP_SYS_WAKE3_PORTP2_MUX  ((uint16_t) ((uint16_t) 1<<2))
#define SWD0_SWD0_CLK_PORTP0_MUX  ((uint16_t) ((uint16_t) 0<<12))
#define SWD0_SWD0_DATA_PORTP0_MUX  ((uint16_t) ((uint16_t) 0<<14))
//#define ADC0_IN_ADC0_IN0_PORTP2_MUX  ((uint16_t) ((uint16_t) 1<<6))
//#define ADC0_IN_ADC0_IN1_PORTP2_MUX  ((uint16_t) ((uint16_t) 1<<8))

int32_t adi_initpinmux(void);

/*
 * Initialize the Port Control MUX Registers
 */
int32_t adi_initpinmux(void) {
    /* PORTx_MUX registers */
    *((volatile uint32_t *)REG_GPIO0_CFG) = SPI0_SCLK_PORTP0_MUX | SPI0_MOSI_PORTP0_MUX
     | SPI0_MISO_PORTP0_MUX  | I2C0_SCL0_PORTP0_MUX
     | I2C0_SDA0_PORTP0_MUX | UART0_TX_PORTP0_MUX | UART0_RX_PORTP0_MUX
     |  SYS_WAKEUP_SYS_WAKE2_PORTP0_MUX | SWD0_SWD0_CLK_PORTP0_MUX
     | SWD0_SWD0_DATA_PORTP0_MUX;//SYS_WAKEUP_SYS_WAKE0_PORTP0_MUX | | SPI0_CS_0_PORTP0_MUX
    *((volatile uint32_t *)REG_GPIO1_CFG) = SPI0_CS_1_PORTP1_MUX | SPI1_SCLK_PORTP1_MUX
     | SPI1_MISO_PORTP1_MUX | SPI1_MOSI_PORTP1_MUX ;//| SPI1_CS_0_PORTP1_MUX;
     //| SYS_WAKEUP_SYS_WAKE1_PORTP1_MUX;
    *((volatile uint32_t *)REG_GPIO2_CFG) = SYS_WAKEUP_SYS_WAKE3_PORTP2_MUX;
     //| ADC0_IN_ADC0_IN0_PORTP2_MUX | ADC0_IN_ADC0_IN1_PORTP2_MUX | SPI0_CS_2_PORTP2_MUX  

    return 0;
}

