/*
* @file     int_puck.c
*
* @brief    Functions for the interruptions
*
* @details  configuration, actions, ...
*/

//#include "common.h"
#include "puck_int.h"
#include <stdio.h>
#include <i2c\I2C.h>
#include <uart\UART_RingBuffer_Puck.h>

ADI_INT_HANDLER_PTR int362_handler;
//void (*ptr)() = sys_wakeup;//function to launch when the interrupt happened


/* for the reset */
#include "startup.h"

/* for the RTC */
uint8_t RtcDevMem0[ADI_RTC_MEMORY_SIZE];
ADI_RTC_HANDLE hDevice0;
uint32_t RTC_count=0x00000000;//beginning for the count
extern uint32_t RTC_alarm;//=0x000000A;//moment to generate the alarm => can be cfg by the app
/* for the wakeup int of the 362 */
IRQn_Type Irq_362=XINT_EVT1_IRQn;
extern volatile bool_t bHibernateExitFlag;
bool_t fsys_wakeup=false;

/* for the RF int */
IRQn_Type Irq_RF=XINT_EVT0_IRQn;
bool_t fRF=false;

extern uint8_t nb_RTC;
extern bool TIMER;
extern bool ftimer;

void sys_wakeup362_cfg ()
{
  const IRQn_Type eIrq = XINT_EVT1_IRQn;        //ADXL362 on ext1
  const ADI_GPIO_IRQ_TRIGGER_CONDITION cond = ADI_GPIO_IRQ_RISING_EDGE;//configure on high level in the 362 cfg
  
  //adi_int_InstallHandler(INT362_ID, int362_handler, &ptr, true);
  //adi_int_EnableInt (INT362_ID, true);
  
  //adi_gpio_Init((void*)gpioMemory, ADI_GPIO_MEMORY_SIZE); //init the GPIO done in the main (GPIO conf)
  adi_gpio_EnableExIRQ(eIrq, cond);//allow wakeup int on EXT1
  adi_gpio_InputEnable(ADI_GPIO_PORT1, ADI_GPIO_PIN_0, true); //enable the input on the P1_00

  //need to configure the call back on event 
  //adi_gpio_RegisterCallback (const IRQn_Type eIrq, ADI_CALLBACK const pfCallback, void *const pCBParam )
  adi_gpio_RegisterCallback (eIrq,  sys_wakeup, &Irq_362);
  
}


void sys_wakeup (void* pCBParam, uint32_t port,  void* pPins)
{
  //put pin high for the supply of the ADXL
  //read the adxl
  //transmission
  //put the pin down?
  //go back into hibernate
  
//  if(pCBParam==hDevice0)
//  {
//    adi_rtc_ClearInterruptStatus(hDevice0,ADI_RTC_ALARM_INT);
//    nb_RTC ++;
//  }
//  else
//  {
//    TIMER=true;
//    if (ftimer)
//    {
//      timer_wakeup_cfg();
//      ftimer=false;
//    }
//  }
  
//  int16_t X372[3], Y372[3], Z372[3];
  
  adi_gpio_DisableExIRQ(XINT_EVT1_IRQn);
  //printf("int 362 disable\n");

  adi_pwr_ExitLowPowerMode(&bHibernateExitFlag);
  //send_UART("\n exit low power mode\n");
  fsys_wakeup=true;
  RTC_count=0x00000000;
}


void timer_wakeup_cfg()
{  
  //open the RTC
  adi_rtc_Open(RTC_DEVICE_NUM,RtcDevMem0,ADI_RTC_MEMORY_SIZE,&hDevice0);
  //function to call on int 
  adi_rtc_RegisterCallback(hDevice0,sys_wakeup,hDevice0);
  //preload
  adi_rtc_SetCount(hDevice0, RTC_count);
  //trim setup                                                  //interval in which we count :
                                                    //count in the range of 2^2s and 2^10s (4s to 17min) with increment +1
  adi_rtc_SetTrim(hDevice0, ADI_RTC_TRIM_INTERVAL_14, ADI_RTC_TRIM_1, ADI_RTC_TRIM_ADD);
  //enable the RTC trim
  adi_rtc_EnableTrim(hDevice0,true);
  //cfg alarm, time to count befor do the alarm
  adi_rtc_SetAlarm(hDevice0, RTC_alarm);
  //enable the alarm
  adi_rtc_EnableAlarm(hDevice0, true);
  //enable the int on the end of the count
  adi_rtc_EnableInterrupts(hDevice0, ADI_RTC_ALARM_INT,true);
  //enable the RTC
  adi_rtc_Enable(hDevice0, true);
  
  //enable int
  //adi_gpio_EnableExIRQ(RTC0_EVT_IRQn, ADI_GPIO_IRQ_RISING_EDGE); //check the cond
  
}


void software_reset()
{
  ResetISR();//function for the reset(in the startup file
}

void RF_int_cfg()
{
  const IRQn_Type eIrq = XINT_EVT0_IRQn;        //RF on evt0
  const ADI_GPIO_IRQ_TRIGGER_CONDITION cond = ADI_GPIO_IRQ_RISING_EDGE;//has to be tested
  
  adi_gpio_EnableExIRQ(eIrq, cond);//allow wakeup int on EXT1
  adi_gpio_InputEnable(ADI_GPIO_PORT0, ADI_GPIO_PIN_15, true); //enable the input on the P0_15
  
  adi_gpio_RegisterCallback (eIrq,  RF_ISR, &Irq_RF);
}

void RF_ISR (void* pCBParam, uint32_t port,  void* pPins)
{
  fRF=true;
  //Read UART
  //parser
  adi_gpio_DisableExIRQ(XINT_EVT0_IRQn);
  adi_gpio_DisableExIRQ(XINT_EVT1_IRQn);
  adi_pwr_ExitLowPowerMode(&bHibernateExitFlag);
  
  //test the cases when this interrupt trigger during the sys_wakeup int 
  //and when the sys_wakeup int trigger during this interrupt (RF_ISR) 
}