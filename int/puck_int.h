/*
* @file     int_puck.h
*
* @brief    Functions for the interruptions
*
* @details  configuration, actions, ...
*/

#ifndef _INT_PUCK_H
#define _INT_PUCK_H

//#include "adi_int.h"
#include "adi_gpio.h"
//#include "adi_tmr.h"
#include "adi_rtc.h"

#define INT362_ID (2)
#define RTC_DEVICE_NUM    0


//function to configure the external wakeup form the ADXL 362
void sys_wakeup362_cfg ();
void test();
//function to deal with the sys wakeup
void sys_wakeup (void*, uint32_t, void*);

//function to initiate the timer0 in wakeup/RTC mode
void timer_wakeup_cfg();


//reset
void software_reset();


//RF interruption configuration
void RF_int_cfg();


//RF routine for the interruption
void RF_ISR (void* pCBParam, uint32_t port,  void* pPins);


#endif