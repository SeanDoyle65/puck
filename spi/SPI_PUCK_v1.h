/*
******************************************************
******************************************************

SPI ADxL362

******************************************************
******************************************************
*/

#include "adi_spi.h" 
 
/** define size of data buffers, DMA max size is 255 */
#define BUFFERSIZE 50u

/* SPI device number which act as MASTER */
#define SPI_MASTER_DEVICE_NUM 0 

/* SPI device number which act as Slave */
//#define SPI_SLAVE_DEVICE_NUM  1 

/* example  result for success */
#define SUCCESS 0

/* example  result for failure */
#define FAILURE 1


void reset_buffer(uint8_t *pBuffer1,uint8_t *pBuffer2);


void InitBuffer_Data(uint8_t *pBuffer,uint8_t data,ADI_SPI_TRANSCEIVER *p);


void InitBuffer_command_write(uint8_t *pBuffer,ADI_SPI_TRANSCEIVER *p);


void InitBuffer_command_read(uint8_t *pBuffer,ADI_SPI_TRANSCEIVER *p);


void InitBuffer_Address(uint8_t *pBuffer,uint8_t data,ADI_SPI_TRANSCEIVER *p);


static void SPICallBack(void* pCBParam, uint32_t Event, void *pArg);


void Init_Hybernate_mode ();

uint16_t make16(uint8_t MSByte, uint8_t LSByte);

void Clock_power_init();

uint16_t Temp_SPI_Read(uint8_t data);

void Config_Master_ADXL();

void Config_Master_ADT();

void Config_ADXL362();

void test3();

void Read_ADT7302();

void Send_Config();

//function to read the 3 axis data in the ADXL 372
void read_ADXL355 (int32_t *Xout, int32_t *Yout, int32_t *Zout); 


void Read_ADXL355_Reg(uint8_t *Reg_data, uint8_t address);
void Write_ADXL355_Reg(uint8_t address, uint8_t data);

//bool Read_Awake_State();

uint16_t make16_362(uint8_t MSByte, uint8_t LSByte);

void Config_SPI_For_ADXL_355();
void Config_And_Run_The_ADXL355() ;

// Alignement functions
void Read_ADXL355_Once(); 
//void Align_ADXL355(float32_t result[]);
void Align_ADXL355(float32_t resultX[], float32_t resultY[], float32_t resultZ[]);
void Single_ADXL_Axis_Read(float32_t result[]);