/*
******************************************************
******************************************************

SPI ADxL362

******************************************************
******************************************************
*/

#include "SPI_PUCK_v1.h"
#include <stddef.h>		/* for 'NULL' */
#include <stdio.h>
#include "System.h"
#include "common.h"
#include "adi_gpio.h"
#include "intrinsics.h" // nop_operation()


/* *************************** Init *******************************************/

/* master buffers */
uint8_t masterTx[BUFFERSIZE];
uint8_t masterRx[BUFFERSIZE];

/* slave buffers */
//uint8_t slaveRx[BUFFERSIZE];

/* Device memory required for operate Master SPI device */
uint8_t MasterSpidevicemem[ADI_SPI_MEMORY_SIZE];

/* Device memory required for operate Slave SPI device */
//uint8_t SlaveSpidevicemem[ADI_SPI_MEMORY_SIZE];

/* Master device handle */
ADI_SPI_HANDLE hMDevice;


bool_t bMasterComplete;
ADI_SPI_RESULT eResult;
uint32_t nResult;
ADI_SPI_TRANSCEIVER Mtransceive;
ADI_SPI_TRANSCEIVER *p=&Mtransceive;

uint8_t MSByte;
uint8_t LSByte;
uint16_t ADC_Temp_Code;
float32_t ADC_Temp_Code_dec,TempVal;

extern volatile bool_t bHibernateExitFlag;
extern uint16_t nbmeas;

uint32_t pnBitrate=0;

/* *************************** Fonction ***************************************/

/* reset the buffer */
void reset_buffer(uint8_t *pBuffer1,uint8_t *pBuffer2)
{
  uint8_t i=0;
  
  for(i=0;i<10;i++)
  {
    pBuffer1[i]=0;
    pBuffer2[i]=0; 
  }
   
}

/* Intialize the buffer with data to be sent */
void InitBuffer_Data(uint8_t *pBuffer,uint8_t data,ADI_SPI_TRANSCEIVER *p)
{
  uint8_t *pt=pBuffer;  // why bother doing this as well extra assign you already have the  pointer 
  uint8_t i=0;
  while (*pt != NULL)
  {
    i++;
    pt++;
  }
  
  pBuffer[i] = data;    // this will always store the data in pBuffer[0] why not use the rest of the space ?
  p->TransmitterBytes++;
  p->ReceiverBytes++;
}

/* Initialize the buffer with write command */
void InitBuffer_command_write(uint8_t *pBuffer,ADI_SPI_TRANSCEIVER *p)
{
  uint8_t *pt=pBuffer;
  uint8_t i=0;
  while (*pt != NULL)
  {
    i++;
    pt++;
  }
  *(pBuffer+i) = 0x0A;
  p->TransmitterBytes++;
  p->ReceiverBytes++;
}

/* Initialize the buffer with read command */
void InitBuffer_command_read(uint8_t *pBuffer,ADI_SPI_TRANSCEIVER *p)
{
  uint8_t *pt=pBuffer;
  uint8_t i=0;
  while (*pt != NULL)
  {
    i++;
    pt++;
  }
 *(pBuffer+i) = 0x0B;
  p->TransmitterBytes++;
  p->ReceiverBytes++;
}

/* Initialize the buffer with address */
void InitBuffer_Address(uint8_t *pBuffer,uint8_t data,ADI_SPI_TRANSCEIVER *p)
{
  uint8_t *pt=pBuffer;
  uint8_t i=0;
  while (*pt != NULL)
  {
    i++;
    pt++;
  }
 *(pBuffer+i) = data;
  p->TransmitterBytes++;
  p->ReceiverBytes++;
}

//static void SPICallBack(void* pCBParam, uint32_t Event, void *pArg)
//{
//  
//  switch(Event)
//    {
//        /* Set the flag to indicate that an interrupt event is detected */
//        case ADI_SPI_EVENT_TRANSFER_COMPLETED:
//          DEBUG_MESSAGE("Transfer completed \n");
//        break;
//          
//        case ADI_SPI_EVENT_CS_EDGE:
//          DEBUG_MESSAGE("CS edge \n");
//        break;
//        
//        case ADI_SPI_EVENT_TX_UNDERFLOW:
//          DEBUG_MESSAGE("Tx underflow \n");
//        break;
//        
//        case ADI_SPI_EVENT_RX_OVERFLOW:
//          DEBUG_MESSAGE("RX Overflow \n");
//        break;
//        
//        case ADI_SPI_EVENT_READY_EDGE:
//          DEBUG_MESSAGE("ready edge \n");
//        break;
//        
//        case ADI_SPI_EVENT_TX_DONE:
//          DEBUG_MESSAGE("Tx done \n");
//        break;
//        
//        case ADI_SPI_EVENT_TX_EMPTY:
//          DEBUG_MESSAGE("Tx empty \n");
//        break;
//        
//        case ADI_SPI_EVENT_BUFFER_PROCESSED:
//          DEBUG_MESSAGE("buffer processed \n");
//        break;
//        
//        default:
//          DEBUG_MESSAGE("Unknown event occurred \n");
//        break;
//    }
//
//    return;
//
//}

uint16_t make16(uint8_t MSByte, uint8_t LSByte)
{
    uint16_t ret =((uint16_t)((MSByte << 8)|LSByte)) ;
    
    return ret;
}

uint16_t make16_362(uint8_t MSByte, uint8_t LSByte)
{
    uint16_t ret =((uint16_t)(((MSByte & 0x0F) << 8)|LSByte));

    
    return ret;
}

void Init_Hybernate_mode ()
{
  
//      ADI_PWR_MODE_ACTIVE         = 0 << BITP_PMG_PWRMOD_MODE,      
//    /*! Core Sleep power-down mode */    
//    ADI_PWR_MODE_FLEXI          = 1 << BITP_PMG_PWRMOD_MODE,      
//    /*! Full Hybernate power-down mode */    
//    ADI_PWR_MODE_HIBERNATE      = 2 << BITP_PMG_PWRMOD_MODE,        
//    /*! System Sleep power-down mode */    
//    ADI_PWR_MODE_SHUTDOWN       = 3 << BITP_PMG_PWRMOD_MODE 

ADI_PWR_RESULT result;
result = adi_pwr_EnterLowPowerMode(ADI_PWR_MODE_HIBERNATE, &bHibernateExitFlag, 0);
DEBUG_RESULT("Failed to HibernateMode",result,ADI_PWR_SUCCESS);

}

void Clock_power_init()
{    
    /* Initialize power service */
    if((adi_pwr_Init())!= ADI_PWR_SUCCESS )
    {
        DEBUG_MESSAGE("Power-init  failed");
        exit(0);
    }
    
    /* Set core clock divider to "1" which sets it to 26Mhz*/
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        exit(0);
    }
    /* Set peripheral clock  divider to "4" which sets it to 26Mhz/4 */    
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        exit(0);
    } 
}


/* Configure the SPI device in Master mode */

ADI_SPI_RESULT ConfigureMasterSPI()
{
    ADI_SPI_RESULT eResult;
    
    /* Open the SPI device. It opens in Master mode by default */
    eResult = adi_spi_Open(SPI_MASTER_DEVICE_NUM,MasterSpidevicemem,ADI_SPI_MEMORY_SIZE,&hMDevice);
    DEBUG_RESULT("Failed to init SPI driver",eResult,ADI_SPI_SUCCESS);
    
    /* Disable DMA Mode */ 
    eResult=adi_spi_EnableDmaMode(hMDevice, false);
    DEBUG_RESULT("Failed to non init DMA",eResult,ADI_SPI_SUCCESS);
    // 1MHz changed to 10 MHZ
    /* Set the bit rate  */
    eResult = adi_spi_SetBitrate(hMDevice, 6000000); 
    DEBUG_RESULT("Failed to set Bitrate",eResult,ADI_SPI_SUCCESS);
    // 1MHz

    return(ADI_SPI_SUCCESS);
    
}


uint16_t Temp_SPI_Read(uint8_t data)
{
    /* reset buffers */
    reset_buffer(masterTx,masterRx);
  
    /* Fill the buffer with values */
    InitBuffer_Data(masterTx,data,p);
    InitBuffer_Data(masterTx,data,p);
    
    /* Prepare the buffers for submitting to Master */     
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.TransmitterBytes = 2;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = masterRx;   
    Mtransceive.ReceiverBytes = 2;
    Mtransceive.nRxIncrement = 1;
    
    adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_3);
     /* Submit the buffer to Master */         
    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);
    
     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
    adi_gpio_SetHigh(ADI_GPIO_PORT0,ADI_GPIO_PIN_3);
    
    uint16_t result = make16(masterRx[0],masterRx[1]);
  
    return  result;

}


void Config_ADXL362()
{
    Config_Master_ADXL();
    
    uint8_t threshold_L=0x3F;
    uint8_t threshold_H=0x00;
    
    // ACTIVITY THRESHOLD REGISTER  -> 
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x20,&Mtransceive);
    InitBuffer_Data(masterTx,threshold_L,&Mtransceive);
    
    Send_Config();
    
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x21,&Mtransceive);
    InitBuffer_Data(masterTx,threshold_H,&Mtransceive);
    
    Send_Config();
    
    // ACTIVITY TIME REGISTER -> 
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x22,&Mtransceive);
    InitBuffer_Data(masterTx,0x01,&Mtransceive);\
    
    Send_Config();
    // INACTIVITY THRESHOLD REGISTER  -> 
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x23,&Mtransceive);
    InitBuffer_Data(masterTx,threshold_L-1,&Mtransceive);
    
    Send_Config();
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x24,&Mtransceive);
    InitBuffer_Data(masterTx,threshold_H,&Mtransceive);
    
    Send_Config();
    
    // INACTIVITY TIME REGISTER -> 1024 /100 -> 10 seconds
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x25,&Mtransceive);
    InitBuffer_Data(masterTx,0x01,&Mtransceive);
    
    Send_Config();
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x26,&Mtransceive);
    InitBuffer_Data(masterTx,0x00,&Mtransceive);    
    
    Send_Config();
    
    // ACTIVITY / INACTIVITY CONTROL REGISTER
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x27,&Mtransceive);
    InitBuffer_Data(masterTx,0x3F,&Mtransceive);
    
    Send_Config();
    
    // FIFO CONTROL REGISTER
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x28,&Mtransceive);
    InitBuffer_Data(masterTx,0x00,&Mtransceive);    
    
    Send_Config();
    
    // FIFO SAMPLES REGISTER
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x29,&Mtransceive);
    InitBuffer_Data(masterTx,0x00,&Mtransceive);
    
    Send_Config();
    
    // FILTER CONTROL REGISTER
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x2C,&Mtransceive);
    InitBuffer_Data(masterTx,0x83,&Mtransceive);
    
    Send_Config();
       
    // POWER CONTROL REGISTER 
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x2D,&Mtransceive);
    InitBuffer_Data(masterTx,0x0E,&Mtransceive);
    
    Send_Config();
    
    // INTMAP1
    
    InitBuffer_command_write(masterTx,&Mtransceive);
    InitBuffer_Address(masterTx,0x2A,&Mtransceive);
    InitBuffer_Data(masterTx,0x40,&Mtransceive);    //0x10
    
    Send_Config();                     
    
    adi_spi_Close (hMDevice);           
}


// only the neccesary registers have been written 
void Config_And_Run_The_ADXL355() 
{
    Write_ADXL355_Reg(0x29,0x03);       // fifo watermark level
    Write_ADXL355_Reg(0x2A,0x02);       // enabling the fifo full interrupt signal 
    Write_ADXL355_Reg(0x2C,0x41);       // setting accelermater range, interrupt polarity and I2C speed
    Write_ADXL355_Reg(0x2D,0x02);       // knocking off temp processing, now in measurement mode
}


void read_ADXL355 (int32_t *Xout, int32_t *Yout, int32_t *Zout)
{

  uint8_t i = 0;     //indicator for the loop, i for axis (3)
  uint8_t ErrorInData = 0;
  int32_t word;
  uint8_t addr = (0x11 <<1)|1;
  bool AlignedData = false;
  bool FifoEmpty = false;

  /* reset buffers */
    reset_buffer(masterTx,masterRx);
    masterTx[0] = addr;
    p->TransmitterBytes = 10;
    p->ReceiverBytes = 10;

    /* Prepare the buffers for submitting to Master */     
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = masterRx;   
    Mtransceive.nRxIncrement = 1;

        
    adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);             // forcing the chip select low
     /* Submit the buffer to Master */         
    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);
    
     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
    adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);          // forcing the chip select high
       
    AlignedData = ((masterRx[3]) & 0x01);
    FifoEmpty =  ((masterRx[3]) & 0x02) | ((masterRx[6]) & 0x02) | ((masterRx[9]) & 0x02);

    
    if (AlignedData == false)
    {
      // this should never happen and is only used to Debug the code
      ErrorInData = 0x1;
    }
    
    if (FifoEmpty == true)
    {
      // this should never happen and is only used to Debug the code
      ErrorInData = 0x2;
    }
    
    if (ErrorInData == 0x00)
    {
      for (i=0; i<3; i++)
      {
       
        word=(masterRx[i*3+1]<<16) & 0xff0000;  // concatenate the 2 byte       
        word+=(masterRx[i*3+2]<<8) & 0xff00;          
        word+=(masterRx[i*3+3]) & 0xff;              
        
        word=(word>>4) & 0x0fffff;   

        if(word & 0x80000 )                     // if the signed bit is at one :
        { 
          word |= 0xfff00000;
        }
        if (i == 0)
        {
          *Xout = word;
        }
        else if (i == 1)
        {
          *Yout = word;
        }
        else if (i == 2)
        {
          *Zout = word;
        }
      }
    }      

    Mtransceive.TransmitterBytes = 0;
    Mtransceive.ReceiverBytes =0;
  }

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  The Function  Read_ADXL355_Reg will readback any register of the ADXL355  //
//  Once you pass it in an array element and the address of the register      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


void Read_ADXL355_Reg(uint8_t *Reg_data, uint8_t address)
{
    uint8_t word = 0; 
    //shifting addresss and making the LSB 1 for a read
    address = (address<<1)|1;   // shifitng the address to the left by 1 and making the LSB bit 1 to enable a read
  
    reset_buffer(masterTx,masterRx);
    masterTx[0] = address;                      // this takes two 8 bit transfer first you send the address 
    masterTx[1] = 0x00;                         // then 0x00 as you dont need to write anything as this is a SPI read 
    p->TransmitterBytes = 2;
    p->ReceiverBytes = 2;

    /* Prepare the buffers for submitting to Master */     
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = masterRx;   
    Mtransceive.nRxIncrement = 1;

    adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);             // forcing the chip select low
     /* Submit the buffer to Master */         
    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);
    
     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
    adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);            // forcing the chip select high
    
    word=(masterRx[1]) & 0xff;                  // concatenate the 2 byte       
      
    *Reg_data = word;                           // storing the readback word in Reg_data
    
    Mtransceive.TransmitterBytes = 0;
    Mtransceive.ReceiverBytes =0;
    
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  functions to read the ADXL355 once to enable you to pull out a single     //
//  axis read   which is used to ensure the Accelerometer is aligned correctly// 
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

void Read_ADXL355_Once()
{
    uint8_t addr = (0x11 <<1)|1;
      
    reset_buffer(masterTx,masterRx);
    masterTx[0] = addr;
    p->TransmitterBytes = 4;
    p->ReceiverBytes = 4;

    /* Prepare the buffers for submitting to Master */     
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = masterRx;   
    Mtransceive.nRxIncrement = 1;

        
    adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);             // forcing the chip select low
     /* Submit the buffer to Master */         
    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);
    
     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
    adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);          // forcing the chip select high
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  function to align the ADXL355 to ensure the captured data is accurate     //
//  everytime you run the code                                                // 
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

void Align_ADXL355(float32_t resultX[], float32_t resultY[], float32_t resultZ[])
{
    int32_t word;
    float32_t word2;

    bool AlignedData = false;
    bool FifoEmpty = false; 
    
    Read_ADXL355_Once();

    AlignedData = ((masterRx[3]) & 0x01);               // checking if the data is aligned
    FifoEmpty =  ((masterRx[3]) & 0x02);                // checking if the fifo is empty
    
    while (AlignedData== false || FifoEmpty == true )
    {
      Read_ADXL355_Once();                              // if the data isnt aligned or the fifo is empty
      AlignedData = ((masterRx[3]) & 0x01);             // we need to keep reading the fifo Register untill the data
      FifoEmpty =  ((masterRx[3]) & 0x02);              // is aligned and the fifo is not empty
    } 
    
    word=(masterRx[1]<<16) & 0xff0000;                  //concatenate the 2 byte       
    word+=(masterRx[2]<<8) & 0xff00;          
    word+=(masterRx[3]) & 0xff;             
    
    word2=(word>>4) & 0x0fffff; 
    resultX[0] = word2;
    resultX[1] = 0;
    
    Single_ADXL_Axis_Read(resultY);                     // getting the Y axis captured sample
    Single_ADXL_Axis_Read(resultZ);                     // and Z Axis samples this is done to ensure we stay aligned
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  This function will grap a single axis which is mainly used to ensure the  //
//  code stays in alignment                                                   // 
//                                                                            //
////////////////////////////////////////////////////////////////////////////////


void Single_ADXL_Axis_Read(float32_t result[])
{
  int32_t word;
  float32_t word2;
  
  Read_ADXL355_Once();

  word=(masterRx[1]<<16) & 0xff0000;        //concatenate the 2 byte       
  word+=(masterRx[2]<<8) & 0xff00;          
  word+=(masterRx[3]) & 0xff;              
  
  word2=(word>>4) & 0x0fffff; 
  result[0] = word2;
  result[1] = 0;
      
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     the Write_ADXL355_Reg will write to any register that can be written   //
//     on the ADXL355                                                         //
////////////////////////////////////////////////////////////////////////////////


void Write_ADXL355_Reg(uint8_t address, uint8_t data )
{
    //shifting addresss
    address = (address<<1);
  
    reset_buffer(masterTx,masterRx);
    masterTx[0] = address;
    masterTx[1] = data;
    p->TransmitterBytes = 2;
    p->ReceiverBytes = 2;

    /* Prepare the buffers for submitting to Master */     
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = masterRx;   
    Mtransceive.nRxIncrement = 1;

    adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);             // forcing the chip select low
     /* Submit the buffer to Master */         
    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);
    
     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
    adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);          // forcing the chip select high
      
    Mtransceive.TransmitterBytes = 0;
    Mtransceive.ReceiverBytes =0;
    
}



void Read_ADT7302()
{
 
    Config_Master_ADT();
    
    ADC_Temp_Code = Temp_SPI_Read(0x00);

    ADC_Temp_Code_dec = (float)ADC_Temp_Code;   //Convert to float for division.

    if ((0x2000 & ADC_Temp_Code) == 0x2000)     //Check sign bit for negative value.
    {
      TempVal = (ADC_Temp_Code_dec - 16384)/32; //Conversion formula if negative temperature.
    }
    else
    {
      TempVal = (ADC_Temp_Code_dec/32);         //Conversion formula if positive temperature.
    }
    
    adi_spi_SetClockPolarity(hMDevice,false);
    adi_spi_SetClockPhase(hMDevice,false);
    if(adi_spi_Close (hMDevice)!= ADI_SPI_SUCCESS)
    {
      printf("fail close spi\n");
    }
}

void Config_Master_ADXL()
{
            
    /* Configure the master */
    eResult = ConfigureMasterSPI();
    DEBUG_RESULT("Failed to configure Master SPI",eResult,ADI_SPI_SUCCESS);

    /* Set the clock polarity idle state high */
    eResult = adi_spi_SetClockPolarity(hMDevice,false);
    DEBUG_RESULT("Failed to init SPI clock polarity",eResult,ADI_SPI_SUCCESS);

    /* Set the clock phase */ 
    eResult = adi_spi_SetClockPhase(hMDevice,false);
    DEBUG_RESULT("Failed to init SPI clock phase",eResult,ADI_SPI_SUCCESS);

    /* Set the chip select */    
    eResult = adi_spi_SetChipSelect (hMDevice, ADI_SPI_CS1);
    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);


    /* Get the bitrate */
//    eResult = adi_spi_GetBitrate (hMDevice,&pnBitrate);
//    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);
    
        
    /* Set the continuous mode*/ 
    eResult = adi_spi_SetContinousMode (hMDevice, true);
    DEBUG_RESULT("Failed to set continuous mode",eResult,ADI_SPI_SUCCESS);

}

void Config_SPI_For_ADXL_355()
{
            
    /* Configure the master */
    eResult = ConfigureMasterSPI();
    DEBUG_RESULT("Failed to configure Master SPI",eResult,ADI_SPI_SUCCESS);
    
    /* Set the clock polarity idle state high */  // This is set to low cpol = 0
    eResult = adi_spi_SetClockPolarity(hMDevice,false);
    DEBUG_RESULT("Failed to init SPI clock polarity",eResult,ADI_SPI_SUCCESS);
    
    /* Set the clock phase */  // cpha =0
    eResult = adi_spi_SetClockPhase(hMDevice,false);
    DEBUG_RESULT("Failed to init SPI clock phase",eResult,ADI_SPI_SUCCESS);
    
    /* Set the chip select */    
    eResult = adi_spi_SetChipSelect (hMDevice, ADI_SPI_CS2);
    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);
    

    /* Get the bitrate */
//    eResult = adi_spi_GetBitrate (hMDevice,&pnBitrate);
//    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);
    
        
    /* Set the continuous mode*/ 
    eResult = adi_spi_SetContinousMode (hMDevice, true);
    DEBUG_RESULT("Failed to set continuous mode",eResult,ADI_SPI_SUCCESS);
    
}

void Config_Master_ADT()
{
            
    /* Configure the master */
    eResult = ConfigureMasterSPI();
    DEBUG_RESULT("Failed to configure Master SPI",eResult,ADI_SPI_SUCCESS);
    
     // configure normal settings and then change idle and CS for master 
    
    /* Set the clock polarity idle state high */ 
    eResult = adi_spi_SetClockPolarity(hMDevice,true);
    DEBUG_RESULT("Failed to init SPI clock polarity",eResult,ADI_SPI_SUCCESS);
    
    /* Set the clock phase */ 
    eResult = adi_spi_SetClockPhase(hMDevice,true);
    DEBUG_RESULT("Failed to init SPI clock phase",eResult,ADI_SPI_SUCCESS);
    
    /* Set the chip select */    
    eResult = adi_spi_SetChipSelect (hMDevice, ADI_SPI_CS0);
    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);
    

    /* Get the bitrate */ 
    eResult = adi_spi_GetBitrate (hMDevice,&pnBitrate);
    DEBUG_RESULT("Failed to set chipselect",eResult,ADI_SPI_SUCCESS);
    
        
    /* Set the continuous mode */ 
    eResult = adi_spi_SetContinousMode (hMDevice, true);
    DEBUG_RESULT("Failed to set continuous mode",eResult,ADI_SPI_SUCCESS);
    
}

void Send_Config()
{
   
    /* Prepare the buffers for submitting to Master */    
  
    Mtransceive.pTransmitter = masterTx;
    Mtransceive.nTxIncrement = 1;
    Mtransceive.pReceiver = NULL;    
    Mtransceive.ReceiverBytes = 0;
    Mtransceive.nRxIncrement =0;
    //Mtransceive.TransmitterBytes auto incremented
    
     /* Submit the buffer to Master */ 

    eResult= adi_spi_MasterTransfer(hMDevice,&Mtransceive);
    DEBUG_RESULT("Master - Data failure",eResult,ADI_SPI_SUCCESS);

     /* Wait till the data transmission is over */
    bMasterComplete = false;
    while( (bMasterComplete == false) )
    {
        adi_spi_MasterComplete(hMDevice,&bMasterComplete);
    }
        
    reset_buffer(masterRx,masterTx);
    Mtransceive.TransmitterBytes = 0;
    Mtransceive.ReceiverBytes =0;
}