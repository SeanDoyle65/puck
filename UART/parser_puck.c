/*
* @file     parser_puck.c
*
* @brief    Function to decode the received data from the smartphone app
*
* @details  decode the sentences and apply the changes to the application
*/

#include "parser_puck.h"
#include <int/puck_int.h>
#include "UART_RingBuffer_Puck.h"


extern uint32_t RTC_alarm;
extern uint32_t RTC_count;
extern ADI_RTC_HANDLE hDevice0;
extern uint8_t nbmeas;
extern bool_t ADXL355, ADXL372, BMP280, ADT7302; 
bool_t domeas=false;
char name[]="PUCK1 "; //when uploading the code on one PUCK board (and not debbuging) put a unique name

/*
 * decode the data received by the UART
 *
*@param char * c : pointer on the sentences to decode
 *
 * @return eresult
 */
void parser ()
{

  char number[5];
  int i=0;
  char c=serInchar();
 
  do
  {

    if(c=='A')
    {
      c=serInchar();
      if(c=='5')
      {
        c=serInchar();
        enable_disable(c,&ADXL355);
      }
      else if(c=='7')
      {
        c=serInchar();
        enable_disable(c,&ADXL372);
      }
      else
        send_UART("wrong input A?",0);
    }
    
    
    else if(c=='D')
    {
      c=serInchar();
      if(c=='M')
        domeas=true;
      else
        send_UART("wrong input D?",0);
    }
    
    else if(c=='I')
    {
      c=serInchar();
      if(c=='D')
        send_UART(name,0);
      else
        send_UART("wrong input I?",0);
    }
    
    
    else if(c=='N')
    { 
      c=serInchar();
      if(c=='M')
      {
        c=serInchar();
        while(c!=' ')
        {
          number[i]=c;
          c=serInchar();
          i++;
        }
        i=0;
        nbmeas=atoi(number);
      }
      
      else
        send_UART("wrong input N?",0);
    }
    
    
    else if(c=='P')
    {
      c=serInchar();
      if (c=='8')
      {
        c=serInchar();
        enable_disable(c,&BMP280);
      }
      else
        send_UART("wrong input P?",0);
    }
    
    
    else if (c=='R')
    {
      c=serInchar();
      if(c=='S')
      {
        c=serInchar();
        if(c=='T')
        {
          send_UART("\ngoing to reset\n",0);
          software_reset();
        }
      }
      
      else if(c=='T')
      {
        c=serInchar();
        if(c=='C')
        {
          c=serInchar();
          while(c!=' ')
          {
            number[i]=c;
            c=serInchar();
            i++;
          }
          i=0;
          RTC_alarm=atoi(number);
          RTC_count=0x00000000;
          adi_rtc_SetAlarm(hDevice0, RTC_alarm);
        }
      }
      else
        send_UART("wrong input R?",0);
    }
    
    
    else if(c=='T')
    {
      c=serInchar();
      if(c=='0')
      {
        c=serInchar();
        enable_disable(c,&ADT7302);
      }
      else
        send_UART("wrong input T?",0);
    }
    
    else 
      send_UART("\nnot recognize by parser\n",0);

    do
    {
      c=serInchar();
    }while (c==' ');
  
  }while(c!='!');
  
}


/*
* enable or disable a device
*
*@param char c : the char from the sentence to decode
*       bool *p : the bool of the device
*
* @return eresult
*/
void enable_disable(char c, bool_t *p)
{
  if (c=='E')
    *p=true;
  else if(c=='D')
    *p=false;
  else
        send_UART("wrong input E/D?",0);
}