/*
* @file     UART_RingBuffer_Puck.c
*
* @brief    Functions for UART using a ring buffer
*
* @details  init, read, write, push, pop, ...
*
*/

#include "UART_RingBuffer_Puck.h"
#include "parser_puck.h"
#include "uart_loop_back.h"
#include "adi_gpio.h"
#include "common.h"

#define       MAX_BUFLEN 32 //RB length


// Structure to handle the ring buffer
	//rb_start: beggining of the buffer
	// rb_end: pend of the buffer
	// rb_in: data to be read
	// rb_out: place to write
#define RB_CREATE(rb)\
   struct {\
     char *rb_start;\
     char *rb_end;\
     char *rb_in;\
     char *rb_out;\
	  }rb

//Init of the pointer struct
// rb: addr of the struct
// start: addr of the first element of the buffer
// number: number of elements of the buffer -1  not sure for the -1
#define RB_INIT(rb, start, number) \
         ( (rb)->rb_in = (rb)->rb_out= (rb)->rb_start= start, \
           (rb)->rb_end = &(rb)->rb_start[number] )

//macro who do the ring
#define RB_SLOT(rb, slot) \
         ( (slot)==(rb)->rb_end? (rb)->rb_start: (slot) )

// Test: Buffer empty? 
#define RB_EMPTY(rb) ( (rb)->rb_in==(rb)->rb_out )

// Test: Buffer full?
#define RB_FULL(rb)  ( RB_SLOT(rb, (rb)->rb_in+1)==(rb)->rb_out )

// Inc of the pointer to the data to be written
#define RB_PUSHADVANCE(rb) ( (rb)->rb_in= RB_SLOT((rb), (rb)->rb_in+1) )

// Inc of the pointer for the data to be read
#define RB_POPADVANCE(rb)  ( (rb)->rb_out= RB_SLOT((rb), (rb)->rb_out+1) )

// Pointer to stock a value in the buffer
#define RB_PUSHSLOT(rb) ( (rb)->rb_in )

// pointer to read a value in the buffer
#define RB_POPSLOT(rb)  ( (rb)->rb_out )
           
           
/* Transmission and Reception Data Buffers */
//static char  outbuf[MAX_BUFLEN];     /* memory for transmission ring buffer #1 (TXD) */
static char  inbuf [MAX_BUFLEN];     /* memory for reception ring buffer #2 (RXD) */


/* define out (transmission)  and in (reception)  ring buffer control structures */
//static RB_CREATE(out);            /* static struct { ... } out; */
static RB_CREATE(in);            /* static struct { ... } in; */



/* Handle for UART device */
#pragma data_alignment=4
ADI_UART_HANDLE          hDevice;

/* Memory for  UART driver */
#pragma data_alignment=4
static uint8_t UartDeviceMem[ADI_UART_BIDIR_MEMORY_SIZE];

#pragma data_alignment=4
static char  nBufferTx0[SIZE_OF_TX_BUFFER];

////////////////////////////////
 void *pProcessedBuffer,*pProcTxBuff0,*pProcRxBuff0;
///////////////////////////////

//#pragma data_alignment=4
//static char nBufferRx0[SIZE_OF_RX_BUFFER];

//#define TRANSFER_SIZE  SIZE_OF_TX_BUFFER
//#define TRANSFER_SIZE  5


/* Variable for storing the return code from UART device */
ADI_UART_RESULT  eUartResult;
/* Bool variable for peek function */
bool bRxBufferComplete=false;
/* Variable to get the buffer address */
void *pProcTxBuff0,*pProcRxBuff0;



/* UART_int */
IRQn_Type Irq_UART=UART_EVT_IRQn;

extern uint16_t nb_harmonic;
extern uint16_t nbmeas;
extern bool ADXL355, ADXL372, ADT7302, BMP280, fsys_wakeup;
extern int pressure;
extern float TempVal;


void init_Serial_Buffer(void) 
{
//  RB_INIT(&out, outbuf, MAX_BUFLEN-1);           /* set up TX ring buffer */
  RB_INIT(&in, inbuf,MAX_BUFLEN-1);             /* set up RX ring buffer */
}

//uint8_t serOutchar(char c)
//{
//  if(!RB_FULL(&out))  // if RB isn't full
//  {                 
//  	*RB_PUSHSLOT(&out) = c;               /* store data in the buffer */
//  	RB_PUSHADVANCE(&out);                 /* adjust write position */
//        Tx0=true;//we can transmit
//        return 0;//complete
//  }
//  return 1;//RB full
//}


char serInchar(void) 
{
  char c;

  if (!RB_EMPTY(&in))
  {                 /* wait for data */

    c = *RB_POPSLOT(&in);                 /* get character off the buffer */
    RB_POPADVANCE(&in);                   /* adjust read position */
    return c;
  }
  return NULL;
}

//uint8_t serOutstring(char *buf) 
//{
//  uint8_t len,code_err=0;
//
//  for(len = 0; len < strlen(buf); len++)
//     code_err +=serOutchar(buf[len]);
//  
//  send_UART();
//  return code_err;
//}

void UART_close()
{
  
  if((eUartResult = adi_uart_Close(&hDevice)) != ADI_UART_SUCCESS)
  {
    DEBUG_MESSAGE("\n\t UART device close  failed");
  }
}

void UART_cfg ()
{
  /* Open the UART device.Data transfer is bidirectional with NORMAL mode by default.  */
  if((eUartResult = adi_uart_Open(UART_DEVICE_NUM,ADI_UART_DIR_TRANSMIT,UartDeviceMem,ADI_UART_BIDIR_MEMORY_SIZE,&hDevice)) != ADI_UART_SUCCESS)
  {
    DEBUG_MESSAGE("\n\t UART device open  failed");
  }
        
  /* Configure  UART device with NO-PARITY, ONE STOP BIT and 8bit word length. */
  if((eUartResult = adi_uart_SetConfiguration(hDevice, ADI_UART_NO_PARITY, ADI_UART_ONE_STOPBIT  , ADI_UART_WORDLEN_8BITS)) != ADI_UART_SUCCESS)
  {
    DEBUG_MESSAGE("UART device configuration failed ");
  }

  /* Baud rate div values are calcuated for PCLK 26Mhz. Please use the
   host utility UartDivCalculator.exe provided with the installer"
  */
  if((eUartResult = adi_uart_ConfigBaudRate(hDevice, UART_DIV_C, UART_DIV_M, UART_DIV_N, UART_OSR)) != ADI_UART_SUCCESS)
  {
    DEBUG_MESSAGE("UART device configuration failed");
  }
  
  if((eUartResult = adi_uart_EnableDMAMode(hDevice,false))!= ADI_UART_SUCCESS)
  {
    DEBUG_MESSAGE("Failed to enable the DMA mode"); 
  }

 // adi_gpio_OutputEnable(ADI_GPIO_PORT0, ADI_GPIO_PIN_10, true);
  
//  adi_uart_SetRxFifoTriggerLevel(hDevice, ADI_UART_RX_FIFO_TRIG_LEVEL_1BYTE);
//  adi_uart_RegisterCallback(hDevice, UARTCallback, &Irq_UART);
//  adi_gpio_EnableExIRQ(UART_EVT_IRQn, ADI_GPIO_IRQ_RISING_EDGE);//check the cond
//  adi_gpio_InputEnable(ADI_GPIO_PORT0, ADI_GPIO_PIN_11, true); //might be pin14 // we want the transmit here 
//  adi_gpio_RegisterCallback (UART_EVT_IRQn,  UARTCallback, &Irq_UART);
}

//need to rethink the receive : impossible to receive when the BLE sleep
//at the end of the send may be let some time to receive?
void UARTreceive()
{
  if(!RB_FULL(&in))
    {
      //receive_UART(reception);
      char c;
      
      do
      {
        receive_UART(&c);
        *RB_PUSHSLOT(&in) = c;        /* store new data in the buffer */
        RB_PUSHADVANCE(&in);               /* next write location */
        
//        if (c!='\n')
//        {
          
//        }
//        else 
//          parser();
//        printf("%c",c);
      }while(c!='!');
    }
  
 }

void makepacket(int *X5,int* Y5, int* Z5,int16_t* X2,int16_t* Y2, int16_t* Z2)
{
  if (fsys_wakeup)
  {
    uint16_t i=0;
    if (ADXL372)
    {  
      for (i=0; i<nbmeas/2; i++)
      { 
        char s[20]={0x01,0x0A, 0x0E};
        for(int j=0;j<2;j++)
        {
          s[3+6*j]=*X2>>8;
          s[4+6*j]=*X2;
          X2++;
          
          s[5+6*j]=*Y2>>8;
          s[6+6*j]=*Y2;
          Y2++;
          
          s[7+6*j]=*Z2>>8;
          s[8+6*j]=*Z2;
          Z2++;
        }
        
        //timestamp
        s[15]=i*2;
        s[16]=i*2+1;
  
        send_UART(s,17);
        for (int j=0; j<10000; j++){__no_operation();}
       }
      if(nbmeas%2!=0)
      {
        char s[20]={0x01,0x0A, 0x07};
        s[3]=*X2>>8;
        s[4]=*X2;
        X2++;
        
        s[5]=*Y2>>8;
        s[6]=*Y2;
        Y2++;
        
        s[7]=*Z2>>8;
        s[8]=*Z2;
        Z2++;
        s[9]=nbmeas;
  
        send_UART(s,10);
      }
    }
    
     if (ADXL355)
    {
      for (i=0; i<nbmeas; i++)
      {
        char s[20]={0x01,0x0B, 0x0D};
        //c=((*X5&0xff000000)>>24);
        //strcat(s,&c);
        s[3]=(*X5>>24);//&0xff000000;
        s[4]=(*X5>>16);//&0x00ff0000;
        s[5]=(*X5>>8);//&0x0000ff00;
        s[6]=*X5;//&0x000000ff;
        X5++;
        

        s[7]=(*Y5>>24);//&0xff000000;
        s[8]=(*Y5>>16);//&0x00ff0000;
        s[9]=(*Y5>>8);//&0x0000ff00;
        s[10]=*Y5;//&0x000000ff;
        Y5++;
        
//        int a=*Z5;
        s[11]=(*Z5>>24);//&0xff000000;
        s[12]=(*Z5>>16);//&0x00ff0000;
        s[13]=(*Z5>>8);//&0x0000ff00;
        s[14]=*Z5;//&0x000000ff;
        Z5++;
        
        //timestamp
        s[15]=i;
//         int b = 2*a;
        send_UART(s,16);
        for (int j=0; j<10000; j++){__no_operation();}
        //printf("\n %s \n",s);
        
       }
    }
    
    
    char s[20]={0x01,0x0C, 0x08};
    if(ADT7302)
    {
      int k;
      float f=TempVal*100;
      k= (int)f;
      s[3]=k>>24;
      s[4]=k>>16;
      s[5]=k>>8;
      s[6]=k;
    }
    else
    {
      s[3]=0;
      s[4]=0;
      s[5]=0;
      s[6]=0;
    }
    if(BMP280)
    {
      s[7]=pressure>>24;
      s[8]=pressure>>16;
      s[9]=pressure>>8;
      s[10]=pressure;
    }
     else
    {
      s[7]=0;
      s[8]=0;
      s[9]=0;
      s[10]=0;
    }
    if (BMP280 || ADT7302)
    {
      send_UART(s,11);
      //printf("\n %s \n",s);
    }
//    char k[20]={0xFF,0xFF, 0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF, 0xFF};
//    send_UART(k,18);
  }
  
  else//for the dm
  {
    if(ADXL372)
    {
      char s[20]={0x01,0x0A, 0x07};
      s[3]=*X2>>8;
      s[4]=*X2;
      X2++;
      
      s[5]=*Y2>>8;
      s[6]=*Y2;
      Y2++;
      
      s[7]=*Z2>>8;
      s[8]=*Z2;
      Z2++;
      s[9]=0;

      send_UART(s,10);
    }
    
     if (ADXL355)
    {
        char s[20]={0x01,0x0B, 0x0D};
        //c=((*X5&0xff000000)>>24);
        //strcat(s,&c);
        s[3]=((*X5&0xff000000)>>24);
        s[4]=(*X5>>16)&0x000000ff;
        s[5]=(*X5>>8)&0x000000ff;
        s[6]=*X5&0x000000ff;
        X5++;
        
        s[7]=(*Y5>>24)&0x000000ff;
        s[8]=(*Y5>>16)&0x000000ff;
        s[9]=(*Y5>>8)&0x000000ff;
        s[10]=*Y5&0x000000ff;
        Y5++;
        
        s[11]=(*Z5>>24)&0x000000ff;
        s[12]=(*Z5>>16)&0x000000ff;
        s[13]=(*Z5>>8)&0x000000ff;
        s[14]=*Z5&0x000000ff;
        Z5++;
        
        //timestamp
        s[15]=0;
        
        send_UART(s,16);
        printf("\n %s \n",s);
    }
  
    char s[20]={0x01,0x0C, 0x08};
    if(ADT7302)
    {
      int k;
      float f=TempVal*100;
      k= (int)f;
      s[3]=k>>24;
      s[4]=k>>16;
      s[5]=k>>8;
      s[6]=k;
    }
    else
    {
      s[3]=0;
      s[4]=0;
      s[5]=0;
      s[6]=0;
    }
    if(BMP280)
    {
      s[7]=pressure>>24;
      s[8]=pressure>>16;
      s[9]=pressure>>8;
      s[10]=pressure;
    }
     else
    {
      s[7]=0;
      s[8]=0;
      s[9]=0;
      s[10]=0;
    }
    if (BMP280 || ADT7302)
    {
      send_UART(s,11);
      printf("\n %s \n",s);
    }
  }

}

void makepacket2(int32_t *transmitX, int32_t *transmitY , int32_t *transmitZ,uint16_t *posX,uint16_t *posY ,uint16_t *posZ ){
  if (fsys_wakeup)
  {
    if (ADXL355)
    {
      float32_t scale= 3999.24/nbmeas;                  //
      char s[362]={0x0B,0x13};
      for (int i=0; i<nb_harmonic; i++)
      {
        
        s[3+(18*i)]=(*transmitX>>24);//&0xff000000;        //send the VALUE
        s[4+(18*i)]=(*transmitX>>16);//&0x00ff0000;
        s[5+(18*i)]=(*transmitX>>8);//&0x0000ff00;
        s[6+(18*i)]=*transmitX;//&0x000000ff;
        transmitX++;
        

        s[7+(18*i)]=(*transmitY>>24);//&0xff000000;           //send the POSITION of the value
        s[8+(18*i)]=(*transmitY>>16);//&0x00ff0000;
        s[9+(18*i)]=(*transmitY>>8);//&0x0000ff00;
        s[10+(18*i)]=*transmitY;//&0x000000ff;
        transmitY++;

        s[11+(18*i)]=(*transmitZ>>24);//&0xff000000;           //send the POSITION of the value
        s[12+(18*i)]=(*transmitZ>>16);//&0x00ff0000;
        s[13+(18*i)]=(*transmitZ>>8);//&0x0000ff00;
        s[14+(18*i)]=*transmitZ;//&0x000000ff;
        
        transmitZ++;
        
        *posX=(*posX)*(scale);  //past to frequency
        *posY=(*posY)*(scale);
        *posZ=(*posZ)*(scale);
        
        s[15+(18*i)]=(*posX>>8);
        s[16+(18*i)]=*posX;
        posX++;
        
        s[17+(18*i)]=(*posY>>8);
        s[18+(18*i)]=*posY;
        posY++;
        
        s[19+(18*i)]=(*posZ>>8);
        s[20+(18*i)]=*posZ;
        posZ++;
      }
        send_UART(s,362); 
      } 
     
     if(ADT7302)
     {
          char s[20]={0x01,0x0C, 0x08};
          int k;
          float f=TempVal*100;
          k= (int)f;
          s[3]=k>>24;
          s[4]=k>>16;
          s[5]=k>>8;
          s[6]=k;
          send_UART(s,7);
     }
    }
 }
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

void send_Data_to_BLE(int32_t *transmitX, int32_t *transmitY , int32_t *transmitZ,uint16_t *posX,uint16_t *posY ,uint16_t *posZ )
{
  if (fsys_wakeup)
  {
    if (ADXL355)
    {
      float32_t scale= 3999.24/nbmeas;                  //
      char s[20]={0x0B,0x13};
      for (int i=0; i<nb_harmonic; i++)
      {
        
        s[3]=(*transmitX>>24);//&0xff000000;        //send the VALUE
        s[4]=(*transmitX>>16);//&0x00ff0000;
        s[5]=(*transmitX>>8);//&0x0000ff00;
        s[6]=*transmitX;//&0x000000ff;
        transmitX++;
        

        s[7]=(*transmitY>>24);//&0xff000000;           //send the POSITION of the value
        s[8]=(*transmitY>>16);//&0x00ff0000;
        s[9]=(*transmitY>>8);//&0x0000ff00;
        s[10]=*transmitY;//&0x000000ff;
        transmitY++;

        s[11]=(*transmitZ>>24);//&0xff000000;           //send the POSITION of the value
        s[12]=(*transmitZ>>16);//&0x00ff0000;
        s[13]=(*transmitZ>>8);//&0x0000ff00;
        s[14]=*transmitZ;//&0x000000ff;
        
        transmitZ++;
        
        *posX=(*posX)*(scale);  //past to frequency
        *posY=(*posY)*(scale);
        *posZ=(*posZ)*(scale);
        
        s[15]=(*posX>>8);
        s[16]=*posX;
        posX++;
        
        s[17]=(*posY>>8);
        s[18]=*posY;
        posY++;
        
        s[19]=(*posZ>>8);
        s[20]=*posZ;
        posZ++;
  
        send_UART(s,20); 
      }

    } 
     
     if(ADT7302)
     {
          char s[20]={0x01,0x0C, 0x08};
          int k;
          float f=TempVal*100;
          k= (int)f;
          s[3]=k>>24;
          s[4]=k>>16;
          s[5]=k>>8;
          s[6]=k;
          send_UART(s,7);
     }
    }
 }





//////////////////////////////////////////////////////////////////////////////////
//                                                                              //
//  makepacket_transmit this function will only send the data of one axis       //
//  sends all the data in on big lump                                           //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////

void makepacket_transmit(int32_t *transmit, uint16_t *pos)
{
  if (ADXL355)
  {
     float32_t scale= 3999.24/nbmeas;

    char s[122]={0x0B,0x13};
    for (int i=0; i<nb_harmonic; i++)
    {
      s[3+(6*i)]=(*transmit>>24);       //send the VALUE
      s[4+(6*i)]=(*transmit>>16);
      s[5+(6*i)]=(*transmit>>8);
      s[6+(6*i)]=*transmit;
      transmit++;

      *pos=(*pos)*(scale);  //past to frequency

      s[7+(6*i)]=(*pos>>8);
      s[8+(6*i)]=*pos;
      pos++;
    }
    send_UART(s,122); 
  }
} 

//////////////////////////////////////////////////////////////////////////////////
//                                                                              //
//  makepacket_temp this function will only send the tempeture data             //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////

void makepacket_temp()
{
     if(ADT7302)
     {
          char s[7]={0x01,0x0C, 0x08};
          int k;
          float f=TempVal*100;
          k= (int)f;
          s[3]=k>>24;
          s[4]=k>>16;
          s[5]=k>>8;
          s[6]=k;
          send_UART(s,7);
    }
}

void send_UART(char *p, short int length)
{
  int size=0;

  // pointless loop 
  if (length==0)
  {     
    while (*p!=NULL)
    {
      nBufferTx0[size]=*p;
      p++;
      size++;
    }
    adi_uart_Write(hDevice,nBufferTx0,size);

  }
  adi_uart_Write(hDevice,p,length);
 }

//need to rethink the receive : impossible to receive when the BLE sleep
//at the end of the send may be let some time to receive?
void receive_UART(char *p)
{
  //char c[1];
  adi_uart_Read(hDevice,p,1);
  //return c;
}