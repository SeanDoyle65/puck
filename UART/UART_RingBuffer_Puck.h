/*
* @file     UART_RingBuffer_Puck.h
*
* @brief    Functions for the I2C
*
* @details  configuration, read, write...
*
*/
#ifndef _UART_RINGBUFFER_PUCK_H_
#define _UART_RINGBUFFER_PUCK_H_

#include "common.h"


//function to create and initialize the ring buffers
void init_Serial_Buffer(void);


//function to put a character in the RB
//uint8_t serOutchar(char c);


//function to read a character in the RB
char serInchar(void);


//function to put a string in the RB
//uint8_t serOutstring(char *buf);


//ISR function
void UARTreceive();


//function to make the string to send via bluetooth
void makepacket(int *X5,int* Y5, int* Z5, int16_t* X2,int16_t* Y2, int16_t* Z2);
void makepacket2(int32_t *transmitX, int32_t *transmitY , int32_t *transmitZ,uint16_t *posX,uint16_t *posY , uint16_t *posZ );

void send_Data_to_BLE(int32_t *transmitX, int32_t *transmitY , int32_t *transmitZ,uint16_t *posX,uint16_t *posY ,uint16_t *posZ );

void makepacket_transmit(int32_t *transmit, uint16_t *pos);
void makepacket_temp();


//function to send
void send_UART(char *p, short int length);


//function to receive
void receive_UART(char *p);


//cfg
void UART_cfg();

// close uart 
void UART_close();
#endif 