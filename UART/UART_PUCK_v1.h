
#include "adi_uart.h"

void fill_TxBuffer(char *tx,char data);

void UART_cfg();

void Enable_LoopBack ();

void validate_data (char *Rx, char *Tx);

void UARTCallback( void *pAppHandle, uint32_t nEvent, void *pArg);

void send_UART(char *p);

void receive_UART(char *p);

void receive_UART_cfg();