/*
* @file     parser_puck.h
*
* @brief    functions of the parser
*
*/

#include "common.h"

#ifndef _PARSER_PUCK_H
#define _PARSER_PUCK_H

//parser to decode the data received by the UART
void parser ();


//enable or disable a device (put true or false in the corresponding bool)
void enable_disable(char c, bool_t *p);

#endif 