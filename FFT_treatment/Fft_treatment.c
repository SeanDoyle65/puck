#include <stdio.h>
#include "common.h"
#include "Fft_treatment.h"
#include "math.h"

extern uint16_t nbmeas;

void axes_filter(float32_t result[], int32_t transmit[], uint16_t position[])
{
  int bin_meas;
  bin_meas = nbmeas ;                   // setting up the measurement for the valid fft data set 
  float32_t array[512];                 // need to hard code this for the moment
  float32_t array2[512];                // array2 is only there for debugging if you think there is a wrong value as the array does not change 
  int ref=1;                            // Position
  float high_value=0;                   // Value of the max harmonic

  for (int p = 1; p < bin_meas; p++)    // getting the abs value of each fft result and storing it in an array
  {
    array[p] = (sqrt(result[p*2]*result[p*2]+result[p*2+1]*result[p*2+1]));
    array2[p]= array[p];                // copying the array into array2  so the code can be debugged if an issue occurs
  }

  for(int p = 1; p < bin_meas; p++)     // fft is currently only be run for 256 datapoints mbeas needs to be increased to 1024  as you have a real and imaginary part which gives 512 
  { 
    if(array[p] > high_value)
    {             
      ref=p; 
      high_value = array[p];            // storing the largest value in array into high_value
    }                                   // after this loop we have the biggest harmonique place and value ==> fundamental
  }
                                        // storing the largest found value in array  into the first transmit element      
  transmit[0]=0.00195*array[ref];       // value  *10 cause of precision of INT insteas of float 
  position[0]=ref;                      // position
  array[ref]=0;	                        // zeroing the current larger number to ensure it does not occur again

  for(int k=1;k<20;k++)                 // detection des nb_h autres harnoniques 
  { 
    ref=1;                                                                 
    high_value =0;  

    for(int p = 1; p < bin_meas; p++)   // on trouve le max
    {

      if(array[p] > high_value)         // la il faut verifier que cette nouvelle grande valeur n'est pas deja dans le tableau
      { 
        bool match=true;
        for(int i=0; i<k; i++)        
        {  
          if(position[i]==p)            // checks current data against every maxium to ensure it is not the already recorded
          {
            match=false;
          }
        }
        if(match)
        {
          ref=p;
          high_value = array[p];
        }
      }
    }

    transmit[k]=0.00195*array[ref];;    // stroing the current largest found  value from the previous in transmit
    position[k]=ref; 
    array[ref] =0;                      // zeroing the current larger number to ensure it does not occur again
  }
}
