
#ifndef __DIALOG_SPI_M350_H
#define __DIALOG_SPI_M350_H


#include <services/gpio/adi_gpio.h>


#define BLE_RST_PIN     ADI_GPIO_PIN_6//4//because on our HW vin0 and vin1 invert
#define BLE_RST_PORT    ADI_GPIO_PORT2


uint32_t adi_Dialog14580_SPI_Boot(uint8_t const * bin, uint32_t length);


void Delay_ms(unsigned int mSec);

#endif