/*!
* @file      I2C.c
*
* @brief     communication between the uC 3029 and ADXL372 and 355 using I2C
*
*/


#include "common.h"
#include "I2C.h"


ADI_I2C_HANDLE I2C_MasterDev;                     //handle to be written    
uint8_t deviceMemory[ADI_I2C_MEMORY_SIZE];    //memory space for the I2C


/**<calibration data for the BMP280*/
static short int dig_T1, dig_T2, dig_T3, dig_P1, dig_P2, dig_P3, dig_P4;
static short int dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;

/*
 * Configure the I2C
 */
void I2C_cfg()
{
	uint32_t i2c_num = 0;  //I2C0 
	
	adi_i2c_Open(i2c_num, ADI_I2C_MASTER, &deviceMemory[0], ADI_I2C_MEMORY_SIZE, &I2C_MasterDev); //initialize I2C in master mode
	adi_i2c_SetBitRate(I2C_MasterDev, 400);       //set Bit rate at 400kHz, without RF we can go from 110kHz to 400kHz
	adi_i2c_SetDutyCycle(I2C_MasterDev, 50);    //duty cycle 50%
	adi_i2c_SetHWAddressWidth(I2C_MasterDev, ADI_I2C_HWADDR_WIDTH_7_BITS);  //component addr on 7b
//        adi_i2c_EnableDMA(I2C_MasterDev, false);     
}


/*
 * Configure the ADXL372
 */
void ADXL372_cfg()
{
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_372);
  
  //no offset => register 0x20 to 0x22 =0x00
  if(WriteInRegister(ID_372, 0x20,0x00) != ADI_I2C_SUCCESS)
  {
//      DEBUG_MESSAGE("adxl372 no offset x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x21,0x00) != ADI_I2C_SUCCESS)
  {
//      DEBUG_MESSAGE("adxl372 no offset y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x22,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no offset z cfg failled\n");
  }
  
  //no threshold=> 0x23 to 0x28 0x0021 (threshold=1 and activity detect enable)
  if(WriteInRegister(ID_372, 0x23, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x24, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x25, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x26, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x27, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold z cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x28, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 no threshold z cfg failled\n");
  }
  
  //activity time register
  //for any activity (any duration we saved it)
  if(WriteInRegister(ID_372, 0x29, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 act time cfg failled\n");
  }
  
  //no inactivity threshold 0x2A to 0x2F=0x00
  if(WriteInRegister(ID_372, 0x2A, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x2B, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x2C, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x2D, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x2E, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs z cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x2F, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact threhs z cfg failled\n");
  }
  
  //inact time // this is a very lonmg time to wait for inactivity 
  if(WriteInRegister(ID_372, 0x30, 0xFF) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact time cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x31, 0xFF) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 inact time cfg failled\n");
  }
  
  //OTN 
  if(WriteInRegister(ID_372, 0x32, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x33, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN x cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x34, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x35, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN y cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x36, 0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN z cfg failled\n");
  }
  if(WriteInRegister(ID_372, 0x37, 0x21) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 OTN z cfg failled\n");
  }
  
  //high pass filter
  //can be change
  if(WriteInRegister(ID_372, 0x38, 0x03) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 HPF cfg failled\n");
  }
  
  //FIFO sample
  //max samples
  if(WriteInRegister(ID_372, 0x39, 0xFF) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 FIFO sample cfg failled\n");
  }
  
  //FIFO ctl
  //max samples => b0=1
  //store x,y,z axis=>b5-3=000          0000 0011
  //stream mode=>b2-1=01
  if(WriteInRegister(ID_372, 0x3A, 0x03) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 FIFO ctl cfg failled\n");
  }
  
  //no int for now
  
  //timing ctl
  //3200ODR =>b7-5=011
  //b4-2=000 wake up every 52ms
  //no ext clock/sync =00
  if(WriteInRegister(ID_372, 0x3D, 0x60) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 FIFO ctl cfg failled\n");
  }
  
  //meas ctl
  //user overange b7=0
  //no autosleep b6=0
  //b5-4=00 default mode meas   0000 0011
  //b3=0 normal op
  //b2-0=011 1600Hz BW
  if(WriteInRegister(ID_372, 0x3E, 0x03) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 meas ctl cfg failled\n");
  }
  
  //power ctl
  //high speed mode (up to 3.2M) b7=1 (we use 400k)
  //high instant on thresh b5=1
  //b4=1 (16ms because we dissable filters              1011 0011
  //no filters b3-2=00                                  
  // full bw meas mode b1-0=11                          
  if(WriteInRegister(ID_372, 0x3F, 0xB3) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl372 power ctl cfg failled\n");
  }
}
  
 


/*
 * Configure the ADXL355
 */
void ADXL355_cfg()
{
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_355);
  
  //no offset : 0x1E to 0x23 =0x00
  if(WriteInRegister(ID_355, 0x1E,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset x cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x1F,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset x cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x20,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset y cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x21,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset y cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x22,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset z cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x23,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 no offset z cfg failled\n");
  }
  
  //activity enable : 
  //we want act on x,y and z =>b2-0=1
  if(WriteInRegister(ID_355, 0x24,0x07) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 act en cfg failled\n");
  }
   
  //activity threshold :
  if(WriteInRegister(ID_355, 0x25,0x00) != ADI_I2C_SUCCESS)//high thresh
  {
      DEBUG_MESSAGE("adxl355 threshold cfg failled\n");
  }
  if(WriteInRegister(ID_355, 0x26,0x01) != ADI_I2C_SUCCESS)//low thresh
  {
      DEBUG_MESSAGE("adxl355 threshold cfg failled\n");
  }
  

  //activity count :
  //for any activity we saved it
  if(WriteInRegister(ID_355, 0x27,0x01) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 activity count cfg failled\n");
  }
  
  //filter settings:
  //no high pass filter=>000
  //ODR=4000Hz=>0000
  if(WriteInRegister(ID_355, 0x28,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 filter settings cfg failled\n");
  }
  
  //FIFO sample register :
  //we want the maximum value : 0x60 (96 samples)
  if(WriteInRegister(ID_355, 0x29,0x60) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 FIFO samples cfg failled\n");
  }
  
  //no int for now
  
  //data sync
  //no ext clock
  //internal sync
  if(WriteInRegister(ID_355, 0x2B,0x00) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 datasync cfg failled\n");
  }
  
  //i2c cfg
  //fast mode (400kHz) b7=0
  //int active high    b6=1
  //range +-10g         b1-0=01
  if(WriteInRegister(ID_355, 0x2C,0x41) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 i2c cfg failled\n");
  }
  
  //power control :
  //DRDY output not force to 0 b2=0
  //disable temp meas b1=1
  //meas mode act b0=0
  if(WriteInRegister(ID_355, 0x2D,0x02) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("adxl355 power ctrl cfg failled\n");
  }
  
}


/*
 * Write in a register value in the ADXL chosen
 *
 *@param uint8_t reg the register to write in, uint8_t value the value to write in the register
 *
 * @return eresult
 */
ADI_I2C_RESULT WriteInRegister (uint8_t device_ID, uint8_t reg, uint8_t value)
{
    uint8_t WriteBHuffer[2];
    ADI_I2C_RESULT eResult=ADI_I2C_SUCCESS;
    
    /* write register address */
    //WriteBHuffer[0] = device_ID;
    WriteBHuffer[0] = reg;
    WriteBHuffer[1] = value;
    
//    adi_i2c_Enable(I2C_MasterDev, true);
//    adi_i2c_EnableDMA(I2C_MasterDev, true);
    
    if(ADI_I2C_SUCCESS != (eResult = adi_i2c_Write(I2C_MasterDev, WriteBHuffer, 2u)))
    {
//        DEBUG_MESSAGE("adi_i2c_Write failed\n");
        return eResult;
    }
    
//    adi_i2c_EnableDMA(I2C_MasterDev, false);
//    adi_i2c_Enable(I2C_MasterDev, false);
    
    return eResult;
}


/*
 * Read a register value in the ADXL chosen
 */
ADI_I2C_RESULT ReadRegister(uint8_t device_ID, uint8_t reg, uint8_t *value)
{
    uint8_t WriteBHuffer[1];
    ADI_I2C_RESULT eResult=ADI_I2C_SUCCESS;
    
    /* write register address */
   // WriteBHuffer[0] = device_ID;        //according to the schematic in 372 DS, the sending is this way : device -> addr <-> value 
    WriteBHuffer[0] = reg;
    void* pBuffer;
    
    if(ADI_I2C_SUCCESS != (eResult = adi_i2c_SubmitTxBuffer(I2C_MasterDev, WriteBHuffer, 1u, true)))
    {
        DEBUG_MESSAGE("adi_i2c_SubmitTxBuffer failed\n");
        return eResult;
    }
    
    /* read register value */
//    if (device_ID==ID_372 && reg==0x42)//a read in 372 FIFO =>2 byte output
//    {
//        if(ADI_I2C_SUCCESS != (eResult = adi_i2c_SubmitRxBuffer(I2C_MasterDev, value, 2u, false)))
//        {
//            DEBUG_MESSAGE("adi_i2c_SubmitRxBuffer failed\n");
//            return eResult;
//        }
//    }
//    else if (device_ID==ID_355 && reg==0x11)//a read in 355 FIFO =>3 byte output
//    {
//      if(ADI_I2C_SUCCESS != (eResult = adi_i2c_SubmitRxBuffer(I2C_MasterDev, value, 3u, false)))
//        {
//            DEBUG_MESSAGE("adi_i2c_SubmitRxBuffer failed\n");
//            return eResult;
//        }
//    }
//    else
//    {
      if(ADI_I2C_SUCCESS != (eResult = adi_i2c_SubmitRxBuffer(I2C_MasterDev, value, 1u, false)))
        {
            DEBUG_MESSAGE("adi_i2c_SubmitRxBuffer failed\n");
            return eResult;
        }
//    }
    
    if (ADI_I2C_SUCCESS != (eResult = adi_i2c_Enable(I2C_MasterDev, true)))
    {
        DEBUG_MESSAGE("adi_i2c_Enable failed\n");
        return eResult;
    }
    if (ADI_I2C_SUCCESS != (eResult = adi_i2c_GetTxBuffer(I2C_MasterDev, &pBuffer)))
    {
        DEBUG_MESSAGE("adi_i2c_GetTxBuffer failed\n");
        return eResult;
    }
    if (ADI_I2C_SUCCESS != (eResult = adi_i2c_GetRxBuffer(I2C_MasterDev, &pBuffer)))
    {
        DEBUG_MESSAGE("adi_i2c_GetRxBuffer failed\n");
        return eResult;
    }
    if (ADI_I2C_SUCCESS != (eResult = adi_i2c_Enable(I2C_MasterDev, false)))
    {
        DEBUG_MESSAGE("adi_i2c_Enable failed\n");
        return eResult;
    }
    
    return eResult;
}


/**
 * Read the 3 axis data in the ADXL372
 *
 *@param uint16_t * Dout : table (size 3) in which it file Xdata, Ydata, Zdata
 *
 * @return none
 *
 */
void read_ADXL372 (int16_t *Xout, int16_t *Yout, int16_t *Zout)
{
  uint8_t value[6];     //value of the read register
  uint8_t i=0;     //indicator for the loop, i for axis (3), j for the number of reading (2byte for 372)
  int16_t word;        //word use to concatenate the value
  uint16_t msb, lsb;     
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_372);
  
  ReadRegister(ID_372, 0x04, value);//register status
  if (value[0]&0x02==0x02)//b1=FIFO_RDY status(one sample or more is rdy)
  {
    value[0]=0x42;
    if(adi_i2c_Write(I2C_MasterDev, &value[0], 1u) != ADI_I2C_SUCCESS)
        DEBUG_MESSAGE("adi_i2c_Write for read failed\n");

   // for(i=0;i<200;i++){}
    
    if( adi_i2c_Read(I2C_MasterDev, value,6u) != ADI_I2C_SUCCESS)
        DEBUG_MESSAGE("adi_i2c_Read for 355 failed\n");

    for (i=0; i<3; i++)
    {
      
        msb=value[i*2];
        lsb=value[i*2+1];
       
        //lsb=lsb&0xF0;       //the 4 last bits don't contain data
        
        word=(msb << 8) & 0xff00;       //concatenate the 2 byte, cast in short int to have the sign of the acceleration
        word+=(lsb) & 0x00f0;  
        //data is left justified so we don't shift on the right by 4b
       // word=~(word)+1;//the data is two's complement
        word=(word>>4);// & 0x0fff;
//        word=~(word)+1;
        if (word & 0x0800)
        {
          //word &= 0xF7FF;
          word|=0xF000;
//          word=~(word)+1;
//          word/=2;
        }
          //word=~(word)+1;
         // word=(word>>4) & 0x0fff;
        //if (value[i*2+1]&0x01==0x01){}printf("\n X should be : %f\n",(float)word*0.0977);}
        if (i==0)
          *Xout=word;
        else if (i==1)
          *Yout=word;
        else if (i==2)
          *Zout=word;
    }
  }
//  else
//    printf("\nFIFO data isn't rdy\n");
  
} 


/**
 * Read the 3 axis data in the ADXL355
 *
 * @param int * Dout : table (size 3) in which it file Xdata, Ydata, Zdata
 *
 * @return none
 *
 */
//void read_ADXL355 (int32_t *Xout, int32_t *Yout, int32_t *Zout)
//{
//  uint8_t value[9];     //value of the read register
//  uint8_t i=0;//, j=0;     //indicator for the loop, i for axis (3), j for the number of reading (3byte for 355)
//  int32_t word;
//  int32_t msb, lsb, mid;
//  uint8_t addr=0x11;
//  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_355);
//  
////  adi_i2c_Enable(I2C_MasterDev, true);
////  adi_i2c_EnableDMA(I2C_MasterDev, true);
//  
//  ReadRegister(ID_372, 0x04, value);//register status
//  value[0]&=0x01;
//  if (value[0]==0x01)//b0=DATA_RDY status(one sample or more is rdy) 
//  {
//   
//    
//    if(adi_i2c_Write(I2C_MasterDev, &addr, 1u) != ADI_I2C_SUCCESS)
//        DEBUG_MESSAGE("adi_i2c_Write for read failed\n");
//
//    if(adi_i2c_Read(I2C_MasterDev, value,9u) != ADI_I2C_SUCCESS)
//        DEBUG_MESSAGE("adi_i2c_Read for 355 failed\n");    
//    
////    adi_i2c_EnableDMA(I2C_MasterDev, false);
////    adi_i2c_Enable(I2C_MasterDev, false);
//      
//    for (i=0; i<3; i++)
//    {
//      msb=value[i*3];
//      mid=value[i*3+1];
//      lsb=value[i*3+2];
//      //lsb=lsb&0xF0;       //the 4 last bits don't contain data
//      
//      word=(msb<<16) & 0xff0000;        //concatenate the 2 byte       
//      word+=(mid<<8) & 0xff00;
//      word+=(lsb) & 0xf0;
//      //word=~(word)+1;//two's complement
//      //msb&=0x80;
//      word=(word>>4) & 0xfffff;
//      if(word & 0x80000 )      //if the signed bit is at one :
//      { 
//        word |= 0xfff00000;
//      }
//      if (i==0)
//        *Xout=word;
//      else if (i==1)
//        *Yout=word;
//      else if (i==2)
//        *Zout=word;
////      
////      else      //if the signed bit isn't at one then the number is good
////      {
////        if (i==0)
////          *Xout=word;
////        else if (i==1)
////          *Yout=word;
////        else if (i==2)
////          *Zout=word;
////      }
//      //printf("with ug/LSB accl is : %f\n",(float)word*0.0000153); //the scale is 15.3 ug/LSB
//    }
//  }
////  else
////    printf("\nFIFO data isn't rdy\n");
//  
//}


/**
 * Read the pressure in the BMP280
 *
 * @param in : none  
 *
 * @return int pressure
 *
 */
int read_BMP280 ()
{
  uint8_t WriteBHuffer[1];
  uint8_t pressure[4], temperature[3];
  uint8_t msb, lsb, mid;
  int Pa=0,T=0;
 //according to the schematic in 280 DS, the sending is this way : device -> addr <-> value 
  WriteBHuffer[0] = 0xF7;       //beggining of the data for pressure
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280W);
  adi_i2c_Write(I2C_MasterDev, WriteBHuffer, 1u);
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280R);
  adi_i2c_Read(I2C_MasterDev, pressure, 3u);
  
  msb=pressure[0];
  mid=pressure[1];
  lsb=pressure[2];
  lsb=lsb&0xF0;
  Pa=((int)((msb << 16) | (mid << 8) | lsb)); 
  Pa=(Pa>>4);
//  Pa=((msb<<12) | (mid<<4) | (lsb >>4));
  
  WriteBHuffer[0] = 0xFA;
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280W);
  adi_i2c_Write(I2C_MasterDev, WriteBHuffer, 1u);
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280R);
  adi_i2c_Read(I2C_MasterDev, temperature, 3u);
  
  msb=temperature[0];
  mid=temperature[1];
  lsb=temperature[2];
  lsb=lsb&0xF0;
  T=((int)((msb << 16) | (mid << 8) | lsb)); 
  T=(T>>4);
//  T=((msb<<12) | (mid<<4) | (lsb >>4));
  
  Pa=compensate_pressure(Pa,T);
  
  return Pa;
}
/* read BMP280 from india all function (ex : compensate_prressure_int32) are in the file adsap_sensors 
int32_t v_uncomp_pressure_s32 = 0;
	int32_t v_uncomp_temperature_s32 = 0;
	uint32_t v_comp_pressure_s32 = 0;
	uint32_t v_comp_temperature_s32 = 0;

	if(bmp280_calib_params_not_read)
		result = BMP280_Get_Calib_Params();

	if(result == ADI_ADS_API_SUCCESS)
		result = BMP280_read_uncomp_pressure_temperature(&v_uncomp_pressure_s32, &v_uncomp_temperature_s32);

	if(result == ADI_ADS_API_SUCCESS)
	{*/
		/* read true pressure and temperature*/
	/*  	v_comp_pressure_s32 = bmp280_compensate_pressure_int32(v_uncomp_pressure_s32);
		SensData.BMP280[0] = ((float)v_comp_pressure_s32)/100;	// To send in hPa; Do not devide by 100 to send in Pa
	#ifdef BMP280_READ_TEMP
		v_comp_temperature_s32 = bmp280_compensate_temperature_int32(v_uncomp_temperature_s32);
		SensData.BMP280[1] = ((float)v_comp_temperature_s32)/100;	// Devide by 100 to send in deg C
	#endif
	*/


/**
 * compensate the pressure for the BMP280
 *
* @param in : int P : non compensate pressure
              int T : non compensate pressure
 *
 * @return int pressure
 *
 */
int compensate_pressure (int P, int T)
{

  int x1t,x2t,t_fine;
  x1t=((((T>>3)-((int)dig_T1<<1)))*((int)dig_T2))>>11;
  x2t=(((((T>>4)-((int)dig_T1))*((T>>4)-((int)dig_T1)))>>12)*((int)dig_T3))>>14;
  t_fine=x1t+x2t;
//  printf("t_fine = %d\n", t_fine);
  T=(t_fine*5+128)>>8;
//  printf("T=%f\n",(float)T/100);
  
  int x1p=0,x2p=0,pres=0;
  x1p=(((t_fine)>>1)-(int)64000);
  //calculate x2p
  x2p=(((x1p >> 2) * (x1p>>2))>>11) * ((int)dig_P6);
  x2p+=((x1p*((int)dig_P5))<<1);
  x2p=(x2p>>2) + (((int)dig_P4)<<16);
  //calculate x1p
  x1p=(((dig_P3 * (((x1p>>2) * (x1p>>2))>>13))>>3) + ((((int)dig_P2) * x1p)>>1))>>18;
  x1p=((((32768 + x1p)) * ((int)dig_P1))>>15);
  //calculate pressure
  pres=(((uint32_t)(((int)1048576) - P) - (x2p>>12)))*3125;
  
  //check overflow
  if(P < 0x80000000)
  {
    if(x1p!=0)
    {
      pres=(pres<<1) / ((uint32_t)x1p);
    }
  }
  
  else
  {
    if(x1p!=0)
    {
      pres=(pres / (uint32_t)x1p) * 2;
    }
  }
  
  //calculate x1p
  x1p=(((int)dig_P9) * ((int)(((pres>>3) * (pres>>3))>>13)))>>12;
  //calculate x2p
  x2p=(((int)(pres >> 2)) * ((int)dig_P8))>>13;
  //true pressure
  pres=(uint32_t)((int)pres + ((x1p + x2p + dig_P7)>>4));
  
  return pres;
  
 // code form india => same result
  /*
  int32_t v_x1_u32r = 0;
	int32_t v_x2_u32r = 0;
	uint32_t v_pressure_u32 = 0;

	 //calculate x1
	v_x1_u32r = (((int32_t) t_fine)>> 1) - (int32_t)64000;

	 //calculate x2
	v_x2_u32r = (((v_x1_u32r >> 2) *				\
				(v_x1_u32r >> 2))>> 11) *		\
				((int32_t) dig_P6);

	v_x2_u32r = v_x2_u32r + ((v_x1_u32r *((int32_t) dig_P5))<<1);

	v_x2_u32r = (v_x2_u32r >> 2) +(((int32_t) dig_P4)	<< 16);

	// calculate x1//
	v_x1_u32r = (((dig_P3 *(((v_x1_u32r >> 2) *				\
	(v_x1_u32r >>2))>> 13))>> 3) +	\
	((((int32_t)dig_P2) *	v_x1_u32r) >>1))	>> 18;

	v_x1_u32r = ((((32768 + v_x1_u32r)) *((int32_t) dig_P1)) >> 15);

	// calculate pressure
	v_pressure_u32 = (((uint32_t)(((int32_t)1048576) - P) -	(v_x2_u32r >>12))) * 3125;

	// check overflow
	if (v_pressure_u32 < 0x80000000)
		// Avoid exception caused by division by zero 
		if (v_x1_u32r != 0)
			v_pressure_u32 = (v_pressure_u32 << 1) / ((uint32_t)v_x1_u32r);
		else
			return 0;
	else
		// Avoid exception caused by division by zero 
		if (v_x1_u32r != 0)
		 	v_pressure_u32 = (v_pressure_u32 /	(uint32_t)v_x1_u32r) * 2;
		else
			return 0;

	// calculate x1
	v_x1_u32r = (((int32_t)	dig_P9) *
				((int32_t)(((v_pressure_u32	>> 3)*		\
				(v_pressure_u32 >> 3))	>> 13)))	>> 12;

	// calculate x2
	v_x2_u32r = (((int32_t)(v_pressure_u32	>> 2)) *			\
				((int32_t) dig_P8))>> 13;

	// calculate true pressure
	v_pressure_u32 = (uint32_t)	((int32_t)v_pressure_u32 + ((v_x1_u32r + v_x2_u32r + dig_P7)	>> 4));

	return v_pressure_u32;*/
}


/**
 * Configure the BMP280
 *
 * @param in : none  
 *
 * @return : none
 *
 */
void BMP280_cfg ()
{
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280W);
  //ctrl_meas 0xF4
  //normal mode meas => b1/0=11
  //b7-5=111    T oversampling          1111 1111
  //b4-2=111    P oversqmpling
  if(WriteInRegister(ID_280, 0xF4, 0xFF) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("BMP280 ctrl_meas cfg failled\n");
  }
  
  //cfg 0xF5
  //one meas every 62.5ms => b7-5=001
  //no filter => b4-2=001                       0010 0101
  //no 3 wire SPI => b0=0 (no b1)
  if(WriteInRegister(ID_280, 0xF5, 0x26) != ADI_I2C_SUCCESS)
  {
      DEBUG_MESSAGE("BMP280 cfg cfg failled\n");
  }
  
  get_const_280();
}


/**
 * Read different constant in the BMP280
 *
 * @param in : none  
 *
 * @return : none
 *
 */
void get_const_280 ()
{
 /*     dig_T1    |  0x88 and 0x89   | from 0 : 7 to 8: 15
 *	dig_T2    |  0x8A and 0x8B   | from 0 : 7 to 8: 15
 *	dig_T3    |  0x8C and 0x8D   | from 0 : 7 to 8: 15
 *	dig_P1    |  0x8E and 0x8F   | from 0 : 7 to 8: 15
 *	dig_P2    |  0x90 and 0x91   | from 0 : 7 to 8: 15
 *	dig_P3    |  0x92 and 0x93   | from 0 : 7 to 8: 15
 *	dig_P4    |  0x94 and 0x95   | from 0 : 7 to 8: 15
 *	dig_P5    |  0x96 and 0x97   | from 0 : 7 to 8: 15
 *	dig_P6    |  0x98 and 0x99   | from 0 : 7 to 8: 15
 *	dig_P7    |  0x9A and 0x9B   | from 0 : 7 to 8: 15
 *	dig_P8    |  0x9C and 0x9D   | from 0 : 7 to 8: 15
 *	dig_P9    |  0x9E and 0x9F   | from 0 : 7 to 8: 15*/
  
  int WriteBHuffer[1];
  uint8_t data_calib [24];
  WriteBHuffer[0] = 0x88;
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280W);
  adi_i2c_Write(I2C_MasterDev, WriteBHuffer, 1u);
  
  adi_i2c_SetHardwareAddress(I2C_MasterDev,TARGETADDR_280R);
  adi_i2c_Read(I2C_MasterDev, data_calib, 24u);
  
  dig_T1=((int16_t)(data_calib[1]<<8)|data_calib[0]);
  dig_T2=((int16_t)(data_calib[3]<<8)|data_calib[2]);
  dig_T3=((int16_t)(data_calib[5]<<8)|data_calib[4]);
  
  dig_P1=((int16_t)(data_calib[7]<<8)|data_calib[6]);
  dig_P2=((int16_t)(data_calib[9]<<8)|data_calib[8]);
  dig_P3=((int16_t)(data_calib[11]<<8)|data_calib[10]);
  dig_P4=((int16_t)(data_calib[13]<<8)|data_calib[12]);
  dig_P5=((int16_t)(data_calib[15]<<8)|data_calib[14]);
  dig_P6=((int16_t)(data_calib[17]<<8)|data_calib[16]);
  dig_P7=((int16_t)(data_calib[19]<<8)|data_calib[18]);
  dig_P8=((int16_t)(data_calib[21]<<8)|data_calib[20]);
  dig_P9=((int16_t)(data_calib[23]<<8)|data_calib[22]);
}