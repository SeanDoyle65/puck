/*
* @file     I2C.h
*
* @brief    Functions for the I2C
*
* @details  configuration, read, write...
*
*/
#ifndef _I2C_H
#define _I2C_H

#include "adi_i2c.h"

#define TARGETADDR_372    (0x53u)     /* hardware address for the ADXL372  */
#define ID_372            (0xFAu)     /* ID of the ADXL372 to R/W */
#define TARGETADDR_355    (0x1Du)    //when HW will be fixed it will be 0x1D /* hardware address for the ADXL355 if MISO high it's 0x53u, if it's low then addr=0x1D */
#define ID_355            (0xEDu)     /* ID of the ADXL355 to R/W */
                                      //don't know if it's the good value or just the reset value, register addr=0x02 for both ADXL
#define TARGETADDR_280W   (0x76u)      //write addr is 1110 11X0 X determine by SDO pin which is GND so 0
#define TARGETADDR_280R   (0xF6u)      //read addr is 1110 11X1
#define ID_280            (0x58u) 


//function to configure the I2C
void I2C_cfg();

//function to configure the ADXL372
void ADXL372_cfg(); 

//function to configure the ADXL355
void ADXL355_cfg();

//function to read a register in a sensor (target addr)
ADI_I2C_RESULT WriteInRegister (uint8_t device_ID, uint8_t reg, uint8_t value);

//function to read a register in a sensor (target addr)
ADI_I2C_RESULT ReadRegister(uint8_t device_ID, uint8_t reg, uint8_t *value);

//function to read the 3 axis data in the ADXL 372
void read_ADXL372 (int16_t *Xout, int16_t *Yout, int16_t *Zout);

//function to read the 3 axis data in the ADXL 372
//void read_ADXL355 (int32_t *Xout, int32_t *Yout, int32_t *Zout);

//function to read the pressure
int read_BMP280 ();

//BMP280 configuration
void BMP280_cfg ();  

//function to compensate the pressure and the temperature from the bmp280
int compensate_pressure (int P, int T);

//get the different value in the 280 needed to compensate
void get_const_280 ();

#endif