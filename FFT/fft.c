#include <system.h>
#include <startup.h>
#include <stdio.h>
//#include <cache.h>

#include <math.h>
#include <stdlib.h>
#include "arm_math.h"
#include <adi_processor.h>

#define ADXL_GPIO_PWR
uint8_t flag0 = 0,  val = 0;            //flag1 = 0,
uint8_t * data_ref;
#define SLEEPDEEP_BIT   (1 << 2)    //  SCR Deep Sleep (SLEEPDEEP)
#define PMG_PWR_KEY     0x4859      // PMG Power Key
#define PRD_VAL 255 // 1 second = 0x100; 16 seconds = 0x1000; 256 seconds= 0x10000
#define OC1IRQ 0 // Core Interrupt on OC1

q15_t x_axis_data[170], y_axis_data[170], z_axis_data[170];

void SysWake1(void)
{
   uint32_t uiTemp32;
  //Set Input enable 
  *pREG_GPIO1_IEN |= 0x1;
  //Enable Pull_up
  *((volatile  uint32_t *) 0x40020048) = 0x1;
  //Configure ExtIRQ1
  uiTemp32 = *pREG_XINT0_CFG0;
  uiTemp32 &= ~(0xF0);
  uiTemp32 |= 0x90; //Falling Edge / Put 0x80 for Rising Edge
  *pREG_XINT0_CFG0 = uiTemp32;
  //Enable Interrupt
  NVIC_EnableIRQ(XINT_EVT1_IRQn); 
}
void SysWake3(void)
{
  //Set Input enable 
  *pREG_GPIO2_IEN |= 0x0002;
  *pREG_GPIO2_PE |= 0x0002;
  // Configure Interrupt
  *pREG_XINT0_CFG0 |= 0x9000;                         // Falling Edge 
  //Enable Interrupt
  NVIC_EnableIRQ(XINT_EVT3_IRQn); 
}
void Change_CLKDIV(int val)
{
  uint32_t uiTemp;
  // Change PCLKDIVCNT
  uiTemp = *pREG_CLKG0_CLK_CTL1;
  uiTemp &= ~(BITM_CLKG_CLK_CTL1_PCLKDIVCNT); 
  uiTemp |= (val << BITP_CLKG_CLK_CTL1_PCLKDIVCNT);
  *pREG_CLKG0_CLK_CTL1 = uiTemp;
  // Change HCLKDIVCNT
  uiTemp = *pREG_CLKG0_CLK_CTL1;
  uiTemp &= ~(BITM_CLKG_CLK_CTL1_HCLKDIVCNT); 
  uiTemp |= (val << BITP_CLKG_CLK_CTL1_HCLKDIVCNT);
  *pREG_CLKG0_CLK_CTL1 = uiTemp;
}
void Ent_Hib()
{
   *pREG_PMG0_PWRKEY = PMG_PWR_KEY;    // Key Protection
    *pREG_PMG0_PWRMOD = 0x0002;         // Select Power Mode (Hibernate)
    *((volatile  uint32_t *) 0xE000ED10) |= SLEEPDEEP_BIT;
    // NVIC_EnableIRQ(RTC1_EVT_IRQn);      // Enable RTC1 Event Interrupt
     NVIC_EnableIRQ(XINT_EVT3_IRQn); 
     __WFI();
}
void OPCX_Cfg()
{
  // CR0 Reset
  while(*pREG_RTC1_SR1 & 0x0180); //wait until sync
  *pREG_RTC1_SR0 = 0xFF;
  *pREG_RTC1_CR0 = 0;
  while(!(*pREG_RTC1_SR0 & 0x0080 )); //wait until sync
  // OC Pin Mux
  *pREG_GPIO2_CFG |= (0x3 << BITP_GPIO_CFG_PIN11);
  // Auto Reload
  while(*pREG_RTC1_SR5 != 0);
  *pREG_RTC1_OC1ARL = PRD_VAL;
  *pREG_RTC1_CR4OC = (1 << BITP_RTC_CR4OC_RTCOC1ARLEN) + (0 << BITP_RTC_CR4OC_RTCOC1MSKEN);
    // Initial Compare Value
  *pREG_RTC1_OC1 = (PRD_VAL);
  if(OC1IRQ == 1)
      NVIC_EnableIRQ(RTC1_EVT_IRQn);
    // Enable/Disable OC1
  *pREG_RTC1_CR3OC = (1 << BITP_RTC_CR3OC_RTCOC1EN) | (OC1IRQ << BITP_RTC_CR3OC_RTCOC1IRQEN);

  while(*pREG_RTC1_SR4 != 0x77FF);
  // Count Reset
  while(*pREG_RTC1_SR1 & 0x0600);
  *pREG_RTC1_CNT0 = 0;
  *pREG_RTC1_CNT1 = 0;
  while(!(*pREG_RTC1_SR0 & 0x0400));
  // Start Count
  while(*pREG_RTC1_SR1 & 0x0180); //wait until sync
  *pREG_RTC1_SR0 = 0xFF;
  *pREG_RTC1_CR0 = (1 << BITP_RTC_CR0_CNTEN);
  while(!(*pREG_RTC1_SR0 & 0x0080)); //wait until sync
}
////////////////////////////////////////////////////////////////////////////////
extern uint8_t * data_ref;
extern uint8_t val;
uint32_t j=0, tm_v = 0;
void SPI2_Init(int var)
{ // SPI2 CLK MOSI MISO
  if(var == 1)
  {
    *pREG_GPIO2_OEN &= ~(1 << 10);
    *pREG_GPIO1_OEN &= ~(7 << 2);
  }
  uint32_t uiTemp = *pREG_GPIO1_CFG;
  uiTemp &= ~(BITM_GPIO_CFG_PIN04 | BITM_GPIO_CFG_PIN03 | BITM_GPIO_CFG_PIN02);
  if(var == 1)
    uiTemp |= ((1 << BITP_GPIO_CFG_PIN04) | (1 << BITP_GPIO_CFG_PIN03) | (1 << BITP_GPIO_CFG_PIN02));
  *pREG_GPIO1_CFG = uiTemp;
  // SPI2 CS2
  uiTemp = *pREG_GPIO2_CFG;
  uiTemp &= ~(BITM_GPIO_CFG_PIN10);
  if(var == 1)
    uiTemp |= (2 << BITP_GPIO_CFG_PIN10);
  *pREG_GPIO2_CFG = uiTemp;
  if(var == 1)
  {
    *pREG_SPI2_CS_CTL = (0x1 << 2);
    // SPI2 CFG SCLK
    *pREG_SPI2_DIV = 0;//0x1;
    *pREG_SPI2_CTL = 0x0843;
  }
  if(var == 0)
  {
    *pREG_GPIO2_OEN |= (1 << 10);
    *pREG_GPIO2_CLR |= (1 << 10);
    *pREG_GPIO1_OEN |= (7 << 2);
    *pREG_GPIO1_CLR |= (7 << 2);
  }
}
uint8_t spi_byte_tx(uint8_t Databyte)
{
  while((*pREG_SPI2_FIFO_STAT & 0x0f00))
    *pREG_SPI2_RX;

  *pREG_SPI2_CNT = 1;
  *pREG_SPI2_TX = Databyte;

  while(!(*pREG_SPI2_STAT & BITM_SPI_STAT_XFRDONE));
  *pREG_SPI2_STAT |= BITM_SPI_STAT_XFRDONE;
  
  while(!(*pREG_SPI2_FIFO_STAT & 0x0f00));
  return *pREG_SPI2_RX;
}
void adxl_access(uint8_t enbl)
{
  if(enbl == 1)
  {
    *pREG_SPI2_CS_OVERRIDE = 0x2;
    while((*pREG_SPI2_CS_OVERRIDE != 0x2));
  }
  else if(enbl == 0)
  {
    *pREG_SPI2_CS_OVERRIDE = 0x1;
    while((*pREG_SPI2_CS_OVERRIDE != 0x1));
  }
}

void adxl_soft_reset()
{
  // ADXL Soft Reset
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x1F);
      spi_byte_tx(0x52);
      adxl_access(0);
}
void adxl_activity_config(uint8_t var)
{
  // Activity Config  0x35
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x27);
      spi_byte_tx(var);
      adxl_access(0);
}
void adxl_fifo_config(uint8_t var)
{
  // FIFO Config        0x0f
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x28);
      spi_byte_tx(0x00);
      adxl_access(0);
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x28);
      spi_byte_tx(var);
      adxl_access(0);
}
void adxl_fifo_samples(uint8_t var)
{
  // FIFO Samples               0xff
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x29);
      spi_byte_tx(var);
      adxl_access(0);
}
void adxl_int1_config(uint8_t var)
{
  // ADXL Int 1         0x84
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x2A);
      spi_byte_tx(var);
      adxl_access(0);
}
void adxl_filter_control(uint8_t var)
{
  // Filter Control     0x08
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x2C);
      spi_byte_tx(var);
      adxl_access(0);
}
void adxl_power_ctl(uint8_t var)
{
  // power Control             0x06
      adxl_access(1);
      spi_byte_tx(0x0A);
      spi_byte_tx(0x2D);
      spi_byte_tx(var);
      adxl_access(0);
}
void read_fifo()
{
  j=0;
    data_ref = (uint8_t *)0x20042000;
  *pREG_SPI2_CTL = 0x0803;
  *pREG_SPI2_CNT = 1024;
  *pREG_SPI2_IEN = 7;
  *pREG_SPI2_RD_CTL = (0 << BITP_SPI_RD_CTL_TXBYTES) + 1;
  adxl_access(1);
  while(*pREG_SPI2_FIFO_STAT & BITM_SPI_FIFO_STAT_RX)
        *pREG_SPI2_RX;
  *pREG_SPI2_TX = 0x0D;
  *pREG_SPI2_RX;// dummy read
  while(j < 128)
  {
    if(*pREG_SPI2_STAT & BITM_SPI_STAT_RXIRQ)
    {
      *pREG_SPI2_STAT |= BITM_SPI_STAT_RXIRQ;
      *(data_ref + (8 * j) + 0) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 1) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 2) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 3) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 4) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 5) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 6) = *pREG_SPI2_RX;
      *(data_ref + (8 * j) + 7) = *pREG_SPI2_RX;
      j++;
    }
  }
  adxl_access(0);
  *pREG_SPI2_RD_CTL = 0;
  *pREG_SPI2_IEN = 0;
  *pREG_SPI2_CTL = 0x0843;
}
void read_stat()
{
  *pREG_SPI2_CTL = 0x0803;
  *pREG_SPI2_CNT = 1;
  *pREG_SPI2_RD_CTL = (1 << BITP_SPI_RD_CTL_TXBYTES) + 1;
  adxl_access(1);
  
  while(*pREG_SPI2_FIFO_STAT & BITM_SPI_FIFO_STAT_RX)
        *pREG_SPI2_RX;
  
  *pREG_SPI2_TX = 0x0B;
  *pREG_SPI2_TX = 0x0B;
  
  *pREG_SPI2_RX;// dummy read
  while(!(*pREG_SPI2_STAT & BITM_SPI_STAT_RXIRQ));
  *pREG_SPI2_STAT |= BITM_SPI_STAT_RXIRQ;
  val = *pREG_SPI2_RX;
  adxl_access(0);
  *pREG_SPI2_RD_CTL = 0;
  *pREG_SPI2_CTL = 0x0843;
}

void ChangetoLFXTAL(int en)
{
  if(en == 1){
    //enable LFXTAL
  *pREG_CLKG0_OSC_KEY = 0xCB14;
   *pREG_CLKG0_OSC_CTL = 0x0007;
   while(!(*pREG_CLKG0_OSC_CTL & 0x0400));
  }
  else{
    // Enable LFOSC
    *pREG_CLKG0_OSC_KEY = 0xCB14;
    *pREG_CLKG0_OSC_CTL = 0x0002;
     while(!(*pREG_CLKG0_OSC_CTL & 0x0100));    
  }
}
void EnableHPBuck(int var)
{
    *pREG_PMG0_CTL1 = var;
}
////////////////////////////////////////////////////////////////////////////////

#ifdef PERCLKOFF
 //Set this bit to clock gate the peripherals   
  pADI_CLKGT->CLKCON5 |= BITM_CLKGT_CLKCON5_PERCLKOFF;
  pADI_CLKGT->CLKCON5 &= ~BITM_CLKGT_CLKCON5_PERCLKOFF;    
  pADI_CLKGT->CLKCON5 |= BITM_CLKGT_CLKCON5_PERCLKOFF;
#endif 
#if 0
void cache_setup_operation(bool i_cache_en, bool i_cache_lock, bool d_cache_en, bool d_cache_lock)
{

int setup_word;

/* if(!i_cache_en && i_cache_lock)

  Error();

if(!d_cache_en && d_cache_lock)

  Error();
*/
if(i_cache_en)

  setup_word += ICACHE_EN;

if(i_cache_lock)

  setup_word += ICACHE_LOCK;

if(d_cache_en)

  setup_word += DCACHE_EN;

if(d_cache_lock)

  setup_word += DCACHE_LOCK;

  

// Enter Cache Key

  CacheKey();

  *pREG_FLCC0_CACHE_SETUP  = setup_word;

}
#endif

  //added volatile so that compiler will not optimize the code.
#define PRIM_NUMS 620
volatile int primes[PRIM_NUMS];
#define n_iter   2 

__ramfunc void prime()
 {
    for (int iter = 1; iter < n_iter; iter++)
     {
       primes[0] = 1;
       for (int i = 1; i < PRIM_NUMS;)
       {

         for (int n = primes[i - 1] + 1; ;n++)
         {


           for (int d = 2; d <= n; d++)
           {

             if (n == d)
             {
               primes[i] = n;
               goto nexti;
             }
             if (n%d == 0) break;
           }
         }
       nexti:
         i++;

       }
     }
 } 

void fft_q15(q15_t *data)
{
  arm_cfft_radix4_instance_q15 S[1];
  //cycle count start
  arm_cfft_radix4_init_q15(S,256,0,1);
  arm_cfft_radix4_q15(S,data);
  //cycle count end
}
void xyz_data()
{
  int index =0;
  for(int i = 0; i <170 ;i++){
    if(data_ref[index+1] == 0x00){
    x_axis_data[i] = (data_ref[index+1] << 8);
    x_axis_data[i] |= data_ref[index];

    y_axis_data[i] = (data_ref[index+3] << 8);
    y_axis_data[i] |= data_ref[index+2]; 

    z_axis_data[i] = (data_ref[index+5] << 8);
    z_axis_data[i] |= data_ref[index+4];
    }
    
    if(data_ref[index+1] == 0x40){
    y_axis_data[i] = (data_ref[index+1] << 8);
    y_axis_data[i] |= data_ref[index];
    
    z_axis_data[i] = (data_ref[index+3] << 8);
    z_axis_data[i] |= data_ref[index+2]; 
    
    x_axis_data[i] = (data_ref[index+5] << 8);
    x_axis_data[i] |= data_ref[index+4];
    }
    
    if(data_ref[index+1] == 0x80){
    z_axis_data[i] = (data_ref[index+1] << 8);
    z_axis_data[i] |= data_ref[index];

    x_axis_data[i] = (data_ref[index+3] << 8);
    x_axis_data[i] |= data_ref[index+2];
    
    y_axis_data[i] = (data_ref[index+5] << 8);
    y_axis_data[i] |= data_ref[index+4];
  //  z_axis_data[i+1] = data_ref[index+5];
    }
    index=index+6;
  }
}

////////////////////////////////////////////////////////////////////////////////
#define PMG_TRIM               (*(volatile unsigned long      *) 0x4004c038)
#define TESTPROT               (*(volatile unsigned long      *) 0x4004c030) 

void fft()
{
   SystemInit();
   // Unlock TestProtKey
 TESTPROT = 0x5970418E; 
   PMG_TRIM = 0xFC734; // <11:10>=01;<9:8>=11; (increasing HPBUCK gains) <7:6>=00; <5:4>=11 (increasing regulated PSM ); <3:2> = 01 (increasing regulated POR); <1:0> = 00 
  
   ChangetoLFXTAL(0);
    *pREG_GPIO0_OEN |= 0x02;
    *pREG_GPIO0_SET |= 0x02;
    
    SysWake3();
    uint32_t i;
    data_ref = (uint8_t *)0x20042000;
    
    SPI2_Init(1);               // enable SPI_Pin_Mux
    adxl_soft_reset();          // soft reset
    
    for(i=0;i<0x7FF;i++);
    adxl_activity_config(0x35);
    adxl_fifo_config(0x0B);
    adxl_fifo_samples(0xFF);
    adxl_int1_config(0x84);     // FIFO_watermark int
    adxl_filter_control(0x08);
    adxl_power_ctl(0x06);       // Measurement Mode On
    SPI2_Init(0);               // disable SPI_Pin_Mux
   OPCX_Cfg();                 // Config OPCX

    while(1)
    {        
        Ent_Hib();              // hibernate
        SPI2_Init(1);           // enable SPI_Pin_Mux
        do
        {
            read_stat();        // read status
        }
        while(!(val & 0x4));
        read_fifo();            // Read data
        SPI2_Init(0);               // disable SPI_Pin_Mux
        val = 0;
        xyz_data();
         fft_q15(x_axis_data);
       fft_q15(y_axis_data);
       fft_q15(z_axis_data);
    }
}
////////////////////////////////////////////////////////////////////////////////
//void Ext_Int3_Handler(void)
//{
//     *pREG_XINT0_CLR = 0x8;                        // Clear Interrupt Source
//     while(*pREG_XINT0_CLR & 0x4);
//}
