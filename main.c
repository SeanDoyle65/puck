#include "System.h"
#include "common.h"
#include <i2c/I2C.h>
#include <int/puck_int.h>
#include <spi/SPI_PUCK_v1.h>
#include "adi_gpio.h"
#include <uart/parser_puck.h>
#include <uart/UART_RingBuffer_Puck.h>
#include <BTLE/sps_device_580.h>
#include <BTLE/dialog14580.h>
#include <FFT/arm_math.h>
#include "math.h"
#include "arm_common_tables.h"
#include <FFT_treatment/Fft_treatment.h>

/* main */
bool_t ADXL355=true, ADT7302=true,ADXL372=false, BMP280=false;
void GPIO_cfg();
void init_clk();
void GPIO_cfg_low();
void Devices_on();
void Devices_off();
void Read_Samples_For_ADXL355();
void BLE_Pulse();
void ADXL355_Power_off();                                
void ADT7302_Power_off();
void fft_f32(float32_t *data);

uint16_t nbmeas=512;

#pragma location="never_retained_ram"
float32_t resultX[1024],  resultY[1024], resultZ[1024]; // floa

/* Unused but not practical to remove*/
#pragma location="never_retained_ram"
int16_t X372[1], Y372[1], Z372[1];

#pragma location="bank1_retained_ram"
int32_t X355[512], Z355[512], Y355[512];              
  
/* I2C */

extern ADI_I2C_HANDLE I2C_MasterDev;   //handle to be written        
int pressure;

/*  pinmux */
extern int32_t adi_initpinmux(void);   

/*  pwr mode */
volatile bool_t bHibernateExitFlag=false;


/* puck int */
extern bool_t fsys_wakeup;
extern uint32_t RTC_count;
extern ADI_RTC_HANDLE hDevice0;
extern bool_t fRF;

/*  SPI */
extern uint8_t masterTx[BUFFERSIZE];
extern uint8_t masterRx[BUFFERSIZE];
extern float32_t TempVal;
extern uint8_t MasterSpidevicemem[ADI_SPI_MEMORY_SIZE];
extern ADI_SPI_HANDLE hMDevice;

/* UART */
extern ADI_UART_HANDLE          hDevice;
extern bool_t domeas;

/* wake up timer, value in seconds */
uint32_t RTC_alarm=6;//60 <=> one minute
uint8_t nb_RTC=0;
bool ftimer=false;

/*booleen : true <=> on, false <=>  off*/
bool TIMER=false;
bool SHAKE=true;        // SHAKE is always true, is this needed

uint16_t nb_harmonic=20;

void main ()
{ 
  SystemInit();                 // init sys 
  init_clk();
  adi_initpinmux();             // this also enables most of the peripherals and gpio's that are used

  adi_Dialog14580_SPI_Boot(sps_device_580_bin,IMAGE_SIZE); // Init BLE

  static uint8_t gpioMemory[ADI_GPIO_MEMORY_SIZE];
  adi_gpio_Init(gpioMemory, ADI_GPIO_MEMORY_SIZE); 

  if (TIMER)       
  {
    timer_wakeup_cfg();
  }
 
  adi_gpio_OutputEnable(ADI_GPIO_PORT1,ADI_GPIO_PIN_12, true);

  Delay_ms(10);                // using 10 ms delay instead of 34ms

  init_Serial_Buffer();
  UART_cfg();
  
  if (SHAKE)    
  {
    sys_wakeup362_cfg();
    Config_ADXL362();
  }
  adi_gpio_OutputEnable(ADI_GPIO_PORT0,ADI_GPIO_PIN_3, true);
  adi_gpio_OutputEnable(ADI_GPIO_PORT1,ADI_GPIO_PIN_13, true);
  adi_gpio_OutputEnable(ADI_GPIO_PORT2,ADI_GPIO_PIN_8, true);    
  adi_gpio_OutputEnable(ADI_GPIO_PORT1,ADI_GPIO_PIN_12, true);
  adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);
  adi_gpio_OutputEnable(ADI_GPIO_PORT0,ADI_GPIO_PIN_14, true);
  //adi_gpio_OutputEnable(ADI_GPIO_PORT0,ADI_GPIO_PIN_10, true);// need to enable the GPIO that controls the UART TX 
  //adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_10);
  adi_gpio_OutputEnable(ADI_GPIO_PORT2,ADI_GPIO_PIN_10, true);
  adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_10);              // this is a handy Debug pin on J6 to easly monitor on a scope

  uint16_t positionX[20];
  uint16_t positionY[20];
  uint16_t positionZ[20];
  
  int32_t transmitX[20];
  int32_t transmitY[20];
  int32_t transmitZ[20];

  uint8_t Channel_ID[1] = {0};
  uint8_t Status_Reg[1] = {0};
 
  bool DataReady = false;

  Devices_off();
    
  while (1)
  {    
    if (fsys_wakeup)
    {
      adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_10);
      Devices_on();
    
      if (ADXL355)
      { 
        Delay_ms(1);
        
        Config_SPI_For_ADXL_355();

        while (Channel_ID[0] != 0xAD)                   // Wait for accelerometer to wake up
        {
          Read_ADXL355_Reg(&Channel_ID[0], 0x00);       // ensureing all the registers are working correctly this is a sanity check as it should always work otherwise the register are not configured correctly
        }
     
        Config_And_Run_The_ADXL355(); 

        // checking an actual read has occured
        DataReady = false;                              // resetting this value for next time
        while(DataReady == false )                      // enusuring the x,y & z acceleration have actually been read by the deivce so that the data is valid 
        {
          Read_ADXL355_Reg(&Status_Reg[0], 0x04);
          DataReady = (Status_Reg[0] & 0x01);
        }        
        
        Align_ADXL355(resultX, resultY, resultZ);       // data align procedure.
        Read_Samples_For_ADXL355();                     // storing the captured samples of the X,Y & Z axis in global variables 
        ADXL355_Power_off();                            // dont need the accelermeter anymore so knock it off
      }

     // adi_gpio_SetHigh(ADI_GPIO_PORT0,ADI_GPIO_PIN_14); // BLE on with pulse        
      fft_f32(resultX);                                 // fft of X
     // adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);  // make the pulse low
    
    
      if(ADT7302)
      {
        Read_ADT7302();                                 // first value is old
        Read_ADT7302();                                 // second is always good
        ADT7302_Power_off();                            // dont need the tempeture sensor anymore so knock it off
      //  makepacket_temp();                              // send temp data
      }
     
      axes_filter(resultX , transmitX, positionX);      // sort X data      
    //  makepacket_transmit(transmitX,positionX);         // send X data
      
      fft_f32(resultY);                                 // fft of Y data
      axes_filter(resultY , transmitY, positionY);      // sort Y data
    //  makepacket_transmit(transmitY,positionY);         // send Y data

      fft_f32(resultZ);                                 // fft of Z data
   
      adi_gpio_SetHigh(ADI_GPIO_PORT0,ADI_GPIO_PIN_14); // BLE on with pulse
      axes_filter(resultZ , transmitZ, positionZ);      // sort Z data
      adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);  // make the pulse low
      
    //  makepacket_transmit(transmitZ,positionZ);         // send Z data

     send_Data_to_BLE(transmitX, transmitY , transmitZ, positionX, positionY, positionZ);
    
      if (nb_RTC > 5)                   // timer 
      {
        TIMER=false;
        nb_RTC=0;
      }
      if (TIMER)
      {
        RTC_count=0x00000000;
        adi_rtc_SetCount(hDevice0, RTC_count);
        adi_rtc_SetAlarm(hDevice0, RTC_alarm);
      }
      if (SHAKE)                                                 
      {
        adi_gpio_EnableExIRQ(XINT_EVT1_IRQn, ADI_GPIO_IRQ_RISING_EDGE);
      }
      
      fsys_wakeup=false;     
      Delay_ms(10);                     // Delay for 10ms for BLE to finishe receiving data this might be to quick or to slow
      BLE_Pulse();                      // BLE off

      adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_10);  
    }
    //Devices_off();
    Init_Hybernate_mode();
  }
}

void Devices_on()
{
  if (ADT7302)
  {
    adi_gpio_SetHigh(ADI_GPIO_PORT1,ADI_GPIO_PIN_13);
    adi_gpio_SetHigh(ADI_GPIO_PORT0,ADI_GPIO_PIN_3);
  }

   if (ADXL355)
  {
    adi_gpio_SetHigh(ADI_GPIO_PORT1,ADI_GPIO_PIN_12);
    adi_gpio_SetHigh(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);
  }

   adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);     // BLE setup to be low 
}

void ADXL355_Power_off()                                // Powering off the ADXL355 accelermoeter
{
  adi_gpio_SetLow(ADI_GPIO_PORT1,ADI_GPIO_PIN_12);
  adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);     
}

void ADT7302_Power_off()                                // Powering off the ADT7302 temp sensor
{
    adi_gpio_SetLow(ADI_GPIO_PORT1,ADI_GPIO_PIN_13);
    adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_3);      
}


void Devices_off()
{
  if(ADT7302)
  {
    adi_gpio_SetLow(ADI_GPIO_PORT1,ADI_GPIO_PIN_13);
    adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_3);     // put the CS0 of the SPI0 in low state 
  }

  if(ADXL355)                                           // ADXL355 => GPIO28   = P1_14
  {
    adi_gpio_SetLow(ADI_GPIO_PORT1,ADI_GPIO_PIN_12);
    adi_gpio_SetLow(ADI_GPIO_PORT2,ADI_GPIO_PIN_8);     
  }
}


void fft_f32(float32_t *data)                           // FFT function
{
  arm_cfft_radix2_instance_f32 S[1];
  arm_cfft_radix2_init_f32(S,nbmeas,0,1);
  arm_cfft_radix2_f32(S,data);  
}

void BLE_Pulse()
{
      adi_gpio_SetHigh(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);
      
      for(uint16_t j=0;j<1000;j++)
      {
        __no_operation();
      }
      
      adi_gpio_SetLow(ADI_GPIO_PORT0,ADI_GPIO_PIN_14);  
}

void Read_Samples_For_ADXL355()
{
  uint8_t Status_Reg[2] = {0};
  bool FifoFull = false;
  for (uint16_t SampleNumber = 1; SampleNumber < nbmeas; SampleNumber++)                //  run for 512 samples from 0 to 511
  { 
    Read_ADXL355_Reg(&Status_Reg[0], 0x04); 
    FifoFull = (Status_Reg[0] & 0x02);

    while( FifoFull == false )                  
    {
      Read_ADXL355_Reg(&Status_Reg[0], 0x04);
      FifoFull = (Status_Reg[0] & 0x02);  
    } 

    read_ADXL355(&X355[SampleNumber], &Y355[SampleNumber], &Z355[SampleNumber]);        // Need result[] to store real values in pair indices and imaginary values in odd ones
    resultX[(2*SampleNumber)]=X355[SampleNumber];                                          
    resultX[(2*SampleNumber)+1]=0;                                                   
    resultY[(2*SampleNumber)]=Y355[SampleNumber];               
    resultY[(2*SampleNumber)+1]=0;
    resultZ[(2*SampleNumber)]=Z355[SampleNumber];
    resultZ[(2*SampleNumber)+1]=0;   

  }
  adi_spi_Close (hMDevice);                             // closing the ADXL SPI as we are finished with it 
  
}

void init_clk()
{
  adi_pwr_Init();
  adi_pwr_SetExtClkFreq(26000000);
  adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFXTAL,true);                              //ADI_CLOCK_SOURCE_HFOSC
//  adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFXTAL);
//  adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL,true);                            // leave timer set yo false at the moment not used at the moment.
  adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
  adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1); 
}