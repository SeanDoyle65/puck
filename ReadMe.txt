How to send a message : 

Choose the instruction among those available in the xlsx parser_puck.
/!\ the end of the command MUST BE an exclamation mark "!"

example ok : "A5E!" : enable ADXL355/ "DM !" : do a measurement
example ok : "A5DDM T0E RTC100 NM6 !" space between order ending with a number
example KO : "A5E"
example KO : "A5E DM "


Devices which work on this version : ADXL355 and ADT7302
Devices which are turned off on this version : ADXL372 BMP280

nbmeas default value : 3 
Timer default value : 15s

wake up with ADXL362 is possible but turned off atm